# PMPC

Third-party libraries:

- [args](https://github.com/Taywee/args) v6.0.4 (modified to [support multiple translation units](https://github.com/Taywee/args/issues/21))
- [filesystem/path.h](https://github.com/bin-build/filesystem) commit 89a4bfe7b3bab8fb685760dc8d399874772707b9 (to support convenient path manipulation without boost)

Todo list for overhaul:

- Use [spdlog](https://github.com/gabime/spdlog) (and [fmtlib](http://fmtlib.net/latest/index.html)) for output