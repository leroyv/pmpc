cmake_minimum_required(VERSION 2.6)
project(boost-filesystem)

enable_language(CXX)

################################################################################
# Compiler flags and preprocessor variables
################################################################################

set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wall")

################################################################################
# Source code
################################################################################

# Specify the include directories
include_directories(${CMAKE_SOURCE_DIR}/include)

# Build the Boost components
file(GLOB_RECURSE boost_source_files src/*.cpp)
add_library(boost ${boost_source_files})

################################################################################
# Install
################################################################################

# Compiled library
install(TARGETS     boost
		DESTINATION lib)

# Headers
install(DIRECTORY include/boost
		DESTINATION include)
