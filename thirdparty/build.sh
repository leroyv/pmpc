#!/bin/bash

# This is a partial implementation of realpath, which
# is missing from BSD systems.
function abspath()
{
    pushd . > /dev/null

    if [ -d "$1" ]; then
        cd "$1"
        dirs -l +0
    else
        cd "`dirname \"$1\"`"
        cur_dir=`dirs -l +0`

        if [ "$cur_dir" == "/" ]; then
            echo "$cur_dir`basename \"$1\"`"
        else
            echo "$cur_dir/`basename \"$1\"`"
        fi
    fi

    popd > /dev/null
}

THIRDPARTY_DIR=`pwd`
INSTALL_DIR=`abspath ..`/install

function boost # Build Boost.Filesystem
{
    cd boost
    mkdir -p build/Release
    cd build/Release
    cmake -DCMAKE_INSTALL_PREFIX=$INSTALL_DIR ../..
    make install
    make clean
    cd $THIRDPARTY_DIR
}

function libconfig # Build libconfig
{
    cd libconfig
    autoreconf
    ./configure --prefix=$INSTALL_DIR
    automake
    make install
    make clean
    cd $THIRDPARTY_DIR
}

for i in "$@"
do
case $i in

    boost)
        boost
        shift
    ;;

    libconfig)
        libconfig
        shift
    ;;

    all)
        boost
        libconfig
        shift
    ;;

esac
done
