#include <cmath>
#include "../misc.h"
#include "BasicVector.h"
#include "Point.h"

using msm::misc::sqr;

namespace msm {
	namespace geometry3d {
		/**
			=================
			Point Class
			=================
		*/

		double Point::distance(Point const &point) const {
			return sqrt(sqr(m_x - point.m_x)
			            + sqr(m_y - point.m_y)
			            + sqr(m_z - point.m_z));
		}

		double Point::distance(Plane const &plane) const {
			return fabs(plane.getA() * m_x + plane.getB() * m_y + plane.getC() * m_z + plane.getD())
			       / sqrt(sqr(plane.getA()) + sqr(plane.getB()) + sqr(plane.getC()));
		}

	}
}
