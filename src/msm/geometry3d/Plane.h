#ifndef MSM_GEOMETRY3D_PLANE_H
#define MSM_GEOMETRY3D_PLANE_H

#include <iostream>

namespace msm {
	namespace geometry3d {
		class Plane {
            /*
             * Class representing a plane in 3D space in a cartesian frame.
             */

		public:

			// Constructors
			Plane();
			Plane(double a, double b, double c, double d);
			Plane(double coefficients[4]);

			// Other member functions
			void setCoefficients(double a, double b, double c, double d);
			void setCoefficients(double coefficients[4]);
			//void print(std::ostream &out) const;

			// Accessors
			double getA() const { return m_a; }
			double getB() const { return m_b; }
			double getC() const { return m_c; }
			double getD() const { return m_d; }

		protected:
			double m_a;
			double m_b;
			double m_c;
			double m_d;

		};
	}
}

#endif
