#include "Sphere.h"
#include <iostream>
#include <vector>
#include <boost/algorithm/string.hpp>
#include "../../utilities/convert.h"

/**
    =============
    Sphere Class
    =============
*/

namespace msm {
	namespace geometry3d {
		void Sphere::print(std::ostream &out) const {
			out << "(" << m_center.getX() << ","
			    << m_center.getY() << ","
			    << m_center.getZ() << ","
			    << m_radius << ")";
		}


		void Sphere::setAll(std::string sphereStr) {
			boost::algorithm::erase_first(sphereStr, "(");
			boost::algorithm::erase_first(sphereStr, ")");

			std::vector<std::string> tempVector;
			boost::algorithm::split(tempVector,
			                        sphereStr,
			                        boost::algorithm::is_any_of(","),
			                        boost::algorithm::token_compress_on);

			if (tempVector.size() == 4) {
				(*this).setAll(convertTo<double>(tempVector[0]),
				               convertTo<double>(tempVector[1]),
				               convertTo<double>(tempVector[2]),
				               convertTo<double>(tempVector[3]));
			}
		}
	}
}
