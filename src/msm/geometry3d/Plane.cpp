#include "Plane.h"

namespace msm {
	namespace geometry3d {

		/**
			================
			Plane class
			================
		*/


		// -------------
		// Constructors
		// -------------

		Plane::Plane() {}

		Plane::Plane(double a, double b, double c, double d) {
			(*this).setCoefficients(a, b, c, d);
		}

		Plane::Plane(double coefficients[4]) {
			(*this).setCoefficients(coefficients);
		}

		// ---------------
		// Other member functions
		// ---------------

		void Plane::setCoefficients(double a, double b, double c, double d) {
			m_a = a;
			m_b = b;
			m_c = c;
			m_d = d;
		}

		void Plane::setCoefficients(double coefficients[4]) {
			(*this).setCoefficients(coefficients[0], coefficients[1], coefficients[2], coefficients[3]);
		}

		/* void BasicVector::print(std::ostream &out) const {
			  out << "(" << m_x << "," << m_y << "," << m_z << ")";
			} */
	}
}
