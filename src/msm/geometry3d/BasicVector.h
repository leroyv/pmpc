#ifndef MSM_GEOMETRY3D_BASIC_VECTOR_H
#define MSM_GEOMETRY3D_BASIC_VECTOR_H

#include <iostream>

namespace msm {
	namespace geometry3d {
		class BasicVector {
			/*
			 * Proto-vector class, should be only used to inherit functionality.
			 */

		public:

			// Constructors
			BasicVector();
			BasicVector(double x, double y, double z);
			BasicVector(double coordinates[3]);

			// Other member functions
			void setCoordinates(double x, double y, double z);
			void setCoordinates(double coordinates[3]);
			void print(std::ostream &out) const;

			// Accessors
			double getX() const { return m_x; }
			double getY() const { return m_y; }
			double getZ() const { return m_z; }

		protected:
			double m_x;
			double m_y;
			double m_z;

		};
	}
}

#endif
