#ifndef MSM_GEOMETRY3D_POINT_H
#define MSM_GEOMETRY3D_POINT_H

#include "BasicVector.h"
#include "Vector.h"
#include "Plane.h"

namespace msm {
	namespace geometry3d {
		class Point : public BasicVector {
			/*
			 * Class representing a 3D space point
			 */
		public:

			// Constructorrs
			Point() : BasicVector() {}

			Point(double x, double y, double z) : BasicVector(x, y, z) {}

			Point(double coordinates[3]) : BasicVector(coordinates) {}

			// Accessors
			Vector getPosVector() const { return Vector(m_x, m_y, m_z); }

			// Operators
			Point operator+(const Vector &moveVector) const {
				return Point(m_x + moveVector.m_x,
				             m_y + moveVector.m_y,
				             m_z + moveVector.m_z);
			}

			Point &operator+=(const Vector &moveVector) {
				m_x += moveVector.m_x;
				m_y += moveVector.m_y;
				m_z += moveVector.m_z;
				return (*this);
			}

			Vector operator-(const Point &subtractor) const {
				return Vector(m_x - subtractor.m_x,
				              m_y - subtractor.m_y,
				              m_z - subtractor.m_z);
			}

			Point &operator=(const Point &rhs) {
				m_x = rhs.m_x;
				m_y = rhs.m_y;
				m_z = rhs.m_z;
				return (*this);
			}

			// Other member functions
			double distance(Point const &point) const;

			double distance(msm::geometry3d::Plane const &plane) const;

			void move(Vector &vector) { *this += vector; }

			void move(double x, double y, double z) {
				m_x += x;
				m_y += y;
				m_z += z;
			}
		};
	}
}

#endif
