#ifndef MSM_GEOMETRY3D_SPHERE_H
#define MSM_GEOMETRY3D_SPHERE_H

#include "Point.h"

namespace msm {
	namespace geometry3d {
		class Sphere {
		public:

			// Constructors
			Sphere() { (*this).setAll(0, 0, 0, 0); }

			Sphere(double const x, double const y, double const z, double const r) {
				(*this).setAll(x, y, z, r);
			}

			Sphere(double center[3], double const r) { (*this).setAll(center, r); }

			Sphere(Point const &center, double const r) {
				(*this).setAll(center, r);
			}

			Sphere(std::string const &sphereStr) { (*this).setAll(sphereStr); }

			// Accessors
			const geometry3d::Point &getCenter() const { return m_center; }

			double getRadius() const { return m_radius; }

			// Ohter member functions
			void setAll(double const x, double const y, double const z,
			            double const r) {
				m_center.setCoordinates(x, y, z);
				m_radius = r;
			}
			// WARNING: this function is not fool-proof and does not throw an exception if r <= 0

			void setAll(double center[3], double const r) {
				m_center.setCoordinates(center);
				m_radius = r;
			}

			void setAll(geometry3d::Point const &center, double const r) {
				m_center = center;
				m_radius = r;
			}

			void setAll(std::string sphereStr);

			void print(std::ostream &out) const;

		private:

			geometry3d::Point m_center;
			double m_radius;
		};
	}
}

#endif
