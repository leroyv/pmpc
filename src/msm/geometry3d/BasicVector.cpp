#include "BasicVector.h"

namespace msm {
  namespace geometry3d {

    /**
        ================
        BasicVector class
        ================
    */


    // -------------
    // Constructors
    // -------------

    BasicVector::BasicVector() {}

    BasicVector::BasicVector(double x, double y, double z) {
      (*this).setCoordinates(x,y,z);
    }

    BasicVector::BasicVector(double coordinates[3]) {
      (*this).setCoordinates(coordinates);
    }

    // ---------------
    // Other member functions
    // ---------------

    void BasicVector::setCoordinates(double x, double y, double z) {
      m_x = x;
      m_y = y;
      m_z = z;
    }

    void BasicVector::setCoordinates(double coordinates[3]) {
      (*this).setCoordinates(coordinates[0], coordinates[1], coordinates[2]);
    }

    void BasicVector::print(std::ostream &out) const {
      out << "(" << m_x << "," << m_y << "," << m_z << ")";
    }
  }
}
