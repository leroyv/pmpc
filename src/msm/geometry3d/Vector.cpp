#include <cmath>
#include "../misc.h"
#include "BasicVector.h"
#include "Vector.h"

using msm::misc::sqr;

namespace msm {
	namespace geometry3d {
		/**
			==================
			Vector Class
			==================
		*/

		// ----------
		// Accessors
		// ----------

		double Vector::getLength() const {
			return sqrt(sqr(m_x) + sqr(m_y) + sqr(m_z));
		}

		double Vector::getLengthSqr() const {
			return sqr(m_x) + sqr(m_y) + sqr(m_z);
		}


		Vector &Vector::cross(Vector const &b, Vector &c) const {
			/*
			 * Returns the cross product *this x b. Result store in c, the method returns a reference to c.
			 */
			c.setCoordinates(m_y * b.m_z - m_y * b.m_y,
			                 m_z * b.m_x - m_x * b.m_z,
			                 m_x * b.m_y - m_y * b.m_x);
			return c;
		}


		Vector Vector::cross(Vector const &b) const {
			/*
			 * Same as the previous one, but returns an object.
			 */
			return (Vector(m_y * b.m_z - m_y * b.m_y,
			               m_z * b.m_x - m_x * b.m_z,
			               m_x * b.m_y - m_y * b.m_x));
		}
	}
}
