#ifndef MSM_GEOMETRY3D_VECTOR_H
#define MSM_GEOMETRY3D_VECTOR_H

#include "BasicVector.h"

namespace msm {
	namespace geometry3d {
		class Vector : public BasicVector {
			/*
			 * Represents a 3D space vector.
			 */
		public:

			// Constructors
			Vector() : BasicVector() {}

			Vector(double x, double y, double z) : BasicVector(x, y, z) {}

			Vector(double coordinates[3]) : BasicVector(coordinates) {}

			// Accessors
			double getLength() const;

			double getLengthSqr() const;

			// Operators
			Vector operator+(const Vector &subtractor) const {
				return Vector(m_x + subtractor.m_x,
				              m_y + subtractor.m_y,
				              m_z + subtractor.m_z);
			}

			Vector operator-(const Vector &subtractor) const {
				return Vector(m_x - subtractor.m_x,
				              m_y - subtractor.m_y,
				              m_z - subtractor.m_z);
			}

			// Scalar product
			Vector operator*(const double multiplier) const {
				return Vector(m_x * multiplier,
				              m_y * multiplier,
				              m_z * multiplier);
			}

			// Scalar product
			Vector operator*(const int multiplier) const {
				return Vector(m_x * multiplier,
				              m_y * multiplier,
				              m_z * multiplier);
			}

			// Scalar division
			Vector operator/(const double divisor) const {
				return Vector(m_x / divisor,
				              m_y / divisor,
				              m_z / divisor);
			}

			// Scalar division
			Vector operator/(const int divisor) const {
				return Vector(m_x / divisor,
				              m_y / divisor,
				              m_z / divisor);
			}

			Vector &operator/=(const double divisor) {
				m_x /= divisor;
				m_y /= divisor;
				m_z /= divisor;
				return (*this);
			}

			Vector &operator/=(const int divisor) {
				m_x /= divisor;
				m_y /= divisor;
				m_z /= divisor;
				return (*this);
			}

			// Affectation operator
			Vector &operator=(const Vector &rhs) {
				m_x = rhs.m_x;
				m_y = rhs.m_y;
				m_z = rhs.m_z;
				return (*this);
			}

			// Other member functions
			Vector &normalize() {
				*this /= (*this).getLength();
				return (*this);
			}

			double scalar(Vector const &vector) const {
				return (m_x * vector.m_x
				        + m_y * vector.m_y
				        + m_z * vector.m_z);
			}

			Vector &cross(Vector const &b, Vector &c) const;

			Vector cross(Vector const &b) const;

			// Friend classes
			friend class Point;

		};
	}
}

#endif
