#include <vector>
#include <stdexcept>
#include "Function.h"
#include <iostream>

using std::vector;
using std::cout;
using std::endl;

namespace msm {
	Function::Function() {
		vector<vector<double> > origin(1);
		origin[0].resize(2);
		origin[0][0] = origin[0][1] = 0;
		(*this).setData(origin);
	}

	Function::Function(const vector<vector<double> > &points) {
		(*this).setData(points);
	}

	Function::Function(const std::vector<double> &abscissa, const std::vector<double> &ordinate) {
		(*this).setData(abscissa, ordinate);
	}

// -----------------------------------------------------------------------------

	Function &Function::setData(const std::vector<std::vector<double> > &points) {
		// Check point list size
		for (vector<vector<double> >::const_iterator it = points.begin(); it != points.end(); ++it)
			if ((*it).size() != 2) throw std::length_error("Bad point size");

		// Check abscissa ordering
		for (unsigned int i = 1; i != points.size(); ++i)
			if (points[i][0] <= points[i - 1][0]) throw std::invalid_argument("Bad abscissa ordering");

		// Copy point list
		m_pointsList = points;

		// Set bounds
		m_upperBound = m_lowerBound = m_pointsList[0][0];

		for (vector<vector<double> >::iterator it = m_pointsList.begin(); it != m_pointsList.end(); ++it) {
			if (m_upperBound < (*it)[0]) m_upperBound = (*it)[0];
			if (m_lowerBound > (*it)[0]) m_upperBound = (*it)[0];
		}

		return (*this);
	}


	Function &Function::setData(const std::vector<double> &abscissa, const std::vector<double> &ordinate) {
		if (abscissa.size() != ordinate.size())
			throw std::length_error("Abscissa and ordinate lists lengths do not match");

		vector<vector<double> > pointList;
		pointList.resize(abscissa.size());

		for (int i = 0; i != abscissa.size(); ++i) {
			pointList[i].resize(2);
			pointList[i][0] = abscissa[i];
			pointList[i][1] = ordinate[i];
		}

		return (*this).setData(pointList);
	}


	Function &Function::setIdentity(double lowerBound, double upperBound, int nPoints) {
		vector<double> abscissa(nPoints);
		for (int i = 0; i != nPoints; ++i)
			abscissa[i] = lowerBound + (upperBound - lowerBound) * (double) i / (double) (nPoints - 1);
		return (*this).setIdentity(abscissa);
	}


	Function &Function::setIdentity(std::vector<double> abscissa) {
		return (*this).setData(abscissa, abscissa);
	}


	Function &Function::setIdentity() {
		return (*this).setIdentity((*this).getAbscissa());
	}


	Function &Function::setConstant(double value, double lowerBound, double upperBound, int nPoints) {
		vector<double> abscissa(nPoints);
		for (int i = 0; i != nPoints; ++i)
			abscissa[i] = lowerBound + (upperBound - lowerBound) * (double) i / (double) (nPoints - 1);
		return (*this).setConstant(value, abscissa);
	}


	Function &Function::setConstant(double value, std::vector<double> abscissa) {
		return (*this).setData(abscissa, vector<double>(abscissa.size(), value));
	}

	Function &Function::setConstant(double value) {
		return (*this).setConstant(value, (*this).getAbscissa());
	}



// -----------------------------------------------------------------------------

	double Function::operator()(double x) const {
		int iInf = (*this).searchNeighor(x);
		int iSup = iInf + 1;

		// Compute interpolated value
		return m_pointsList[iInf][1] +
		       (m_pointsList[iSup][1] - m_pointsList[iInf][1]) / (m_pointsList[iSup][0] - m_pointsList[iInf][0]) *
		       (x - m_pointsList[iInf][0]);
	}


	std::vector<double> Function::operator[](int i) const {
		return m_pointsList[i];
	}


	Function Function::operator-() const {
		vector<double> ordinate = (*this).getOrdinate();
		for (vector<double>::iterator it = ordinate.begin(); it != ordinate.end(); ++it) *it = -(*it);
		return (Function((*this).getAbscissa(), ordinate));
	}


	Function &Function::operator*=(const Function &f) {
		// Check: are abscissae lists identical?
		if (f.getAbscissa() != (*this).getAbscissa()) throw std::invalid_argument("Abscissa lists are not identical");

		for (int i = 0; i != f.getPointsList().size(); ++i) {
			m_pointsList[i][1] *= f.getPointsList()[i][1];
		}

		return (*this);
	}


	const Function Function::operator*(const Function &f) const {
		Function g = (*this);
		return (g *= f);
	}


	Function &Function::operator+=(double k) {
		for (vector<vector<double> >::iterator it = m_pointsList.begin(); it != m_pointsList.end(); ++it) (*it)[1] += k;
		return (*this);
	}


	Function &Function::operator-=(double k) {
		for (vector<vector<double> >::iterator it = m_pointsList.begin(); it != m_pointsList.end(); ++it) (*it)[1] -= k;
		return (*this);
	}


	Function &Function::operator*=(double k) {
		for (vector<vector<double> >::iterator it = m_pointsList.begin(); it != m_pointsList.end(); ++it) (*it)[1] *= k;
		return (*this);
	}


	const Function Function::operator+(double k) const {
		Function f = (*this);
		return (f += k);
	}


	const Function Function::operator-(double k) const {
		Function f = (*this);
		return (f -= k);
	}


	const Function Function::operator*(double k) const {
		Function f = (*this);
		return (f *= k);
	}



// -----------------------------------------------------------------------------

	int Function::searchNeighor(double x) const {
		// Detect the interval in which x is located and return the lower bound index for this interval

		// Check that x is in the bounds
		if (x > m_upperBound || x < m_lowerBound) throw std::invalid_argument("Argument out of bounds");

		// Dichotomy search
		int iInf = 0;
		int iSup = m_pointsList.size();
		int iMid = (iSup + iInf) / 2;

		while (iSup - iInf > 1) {
			if (x > m_pointsList[iMid][0]) iInf = iMid;
			else iSup = iMid;
			iMid = (iSup + iInf) / 2;
		}

		return iInf;
	}

	Function Function::diff() {
		// Differtientiate the function

		std::vector<std::vector<double> > diffPointsList(m_pointsList.size() - 1);

		for (int i = 0; i != m_pointsList.size() - 1; ++i) {
			diffPointsList[i].resize(2);
			diffPointsList[i][0] = m_pointsList[i][0];
			diffPointsList[i][1] =
				(m_pointsList[i + 1][1] - m_pointsList[i][1]) / (m_pointsList[i + 1][0] - m_pointsList[i][0]);
		}

		return Function(diffPointsList);
	}

	double Function::integral() const {
		return (*this).integral(0, m_pointsList.size() - 1);
	}

	double Function::integral(int iMin, int iMax) const {
		// Compute integral between iMin and iMax using the trapezium rule
		double integral = 0;

		for (int i = iMin; i != iMax - 1; ++i) {
			integral +=
				0.5 * (m_pointsList[i][1] + m_pointsList[i + 1][1]) * (m_pointsList[i + 1][0] - m_pointsList[i][0]);
		}

		return integral;
	}

// -----------------------------------------------------------------------------

	std::vector<double> Function::getAbscissa() const {
		std::vector<double> myVector(m_pointsList.size());
		for (int i = 0; i != m_pointsList.size(); ++i) myVector[i] = m_pointsList[i][0];
		return myVector;
	}

	std::vector<double> Function::getOrdinate() const {
		std::vector<double> myVector(m_pointsList.size());
		for (int i = 0; i != m_pointsList.size(); ++i) myVector[i] = m_pointsList[i][1];
		return myVector;
	}

}

