#include <iostream>
#include "geometry3d.h"

std::ostream &operator<<( std::ostream &out, msm::geometry3d::BasicVector const &myBasicVector ) {
  myBasicVector.print(out);
  return out;
}

std::ostream &operator<<( std::ostream &out, msm::geometry3d::Sphere const &mySphere ) {
  mySphere.print(out);
  return out;
}


