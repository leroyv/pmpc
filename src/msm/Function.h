/*
 * Basic class representing a function R -> R defined by linear interpolation in a (x, y) points set.
 */

#ifndef MSM_FUNCTION_H
#define MSM_FUNCTION_H

#include <vector>
#include <string>

namespace msm {
  class Function {

    public:

    // Constructors
    Function();
    Function(const std::vector<std::vector<double> > &points);
    Function(const std::vector<double> &abscissa, const std::vector<double> &ordinate);

    

    //Destructor
    //~Function();
    
    // Generation methods
    Function& setData(const std::vector<std::vector<double> > &points);
    Function& setData(const std::vector<double> &abscissa, const std::vector<double> &ordinate);
    Function& setIdentity(double lowerBound, double upperBound, int nPoints = 2);
    Function& setIdentity(std::vector<double> abscissa);
    Function& setIdentity();
    Function& setConstant(double value, double lowerBound, double upperBound, int nPoints = 2);
    Function& setConstant(double value, std::vector<double> abscissa);
    Function& setConstant(double value);
    

    // Operators
//    Function& operator=(const Function &f);
    double operator()(double x) const;
    std::vector<double> operator[](int i) const;
    
    Function operator-() const;
    
    Function& operator*=(const Function& f);
    const Function operator*(const Function& f) const;

    Function& operator+=(double k);
    Function& operator-=(double k);
    Function& operator*=(double k);
    const Function operator+(double k) const;
    const Function operator-(double k) const;
    const Function operator*(double k) const;
    
    
    // Others member functions
    int searchNeighor(double x) const; // Returns the index of the point which is the nearest neighbor to x l'abscisse passée en argument
    Function diff(); // Differentiate
    double integral() const;
    double integral(int iMin, int iMax) const; // Compute integral between iMin and iMax using the trapezium rule

    
    // Accessors
    inline double getLowerBound() const { return m_lowerBound; }
    inline double getUpperBound() const { return m_upperBound; }
    inline std::vector<std::vector<double> > const &getPointsList() const { return m_pointsList; }
    std::vector<double> getAbscissa() const;
    std::vector<double> getOrdinate() const;


    private:

    std::vector<std::vector<double> > m_pointsList; // Points set defining the function
    double m_lowerBound;
    double m_upperBound;
    
  };
}

// Other operators (some to guarantee commutativity)
inline const msm::Function operator+(double k, msm::Function& f) { return f+k; }
inline const msm::Function operator-(double k, msm::Function& f) { return -(f-k); }
inline const msm::Function operator*(double k, msm::Function& f) { return f*k; }

#endif
