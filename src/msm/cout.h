// Stream operator definitions for various classes

#ifndef MSM_COUT_H
#define MSM_COUT_H

#include <iostream>
#include "geometry3d.h"
#include <vector>
#include <boost/any.hpp>

// Points and vectors display
std::ostream &operator<<( std::ostream &out, msm::geometry3d::BasicVector const &myBasicVector );

// Sphere display
std::ostream &operator<<( std::ostream &out, msm::geometry3d::Sphere const &mySphere );

// Vector display
template <typename T>
void printVector(std::ostream &out, std::vector<T> const &myVector) {
  out << myVector[0];
  if ( myVector.size() > 1 ) {
    for ( int i = 1; i != myVector.size(); ++i ) {
      out << "," << myVector[i];
    }
  }
}

template <typename T>
std::ostream &operator<<( std::ostream &out, std::vector<T> const &myVector ) {
  printVector(out, myVector);
  return out;
}

#endif
