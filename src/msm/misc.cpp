#include <vector>
#include <stdexcept>

namespace msm {
	namespace misc {
		// Differentiate a vector
		std::vector<double> diff(std::vector<double> ordinate, std::vector<double> abscissa) {
			if (ordinate.size() != abscissa.size())
				throw (std::length_error("Abscissae and ordinates vectors must have equal sizes."));

			std::vector<double> diffVector;
			for (int i = 0; i != ordinate.size() - 1; ++i)
				diffVector[i] = (ordinate[i + 1] - ordinate[i]) / (abscissa[i + 1] - abscissa[i]);

			return diffVector;
		}
	}
}
