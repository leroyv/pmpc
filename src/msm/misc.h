#ifndef MSM_MISC_H
#define MSM_MISC_H

#include <cmath>
#include <vector>

namespace msm {
	namespace misc {
		// Square
		inline double sqr(double a) { return a * a; }

		inline int sqr(int a) { return a * a; }

		// Cube
		inline double cube(double a) { return a * a * a; }

		inline int cube(int a) { return a * a * a; }

		// Bi-square
		inline double bisqr(double a) { return a * a * a * a; }

		inline int bisqr(int a) { return a * a * a * a; }

		// Sign
		inline int sign(double a) { if (a >= 0) { return 1; } else { return -1; }}

		// Float comparison
		inline bool almostEqualRelative(double A, double B, double maxRelativeError) {
			if (A == B)
				return true;
			float relativeError;

			if (fabs(B) > fabs(A))
				relativeError = fabs((A - B) / B);
			else
				relativeError = fabs((A - B) / A);

			if (relativeError <= maxRelativeError)
				return true;

			return false;
		}

		// Vector differentiation
		std::vector<double> diff(std::vector<double> ordinate, std::vector<double> abscissa);
	}
}

#endif
