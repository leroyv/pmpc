//
// Created by leroy on 7/3/17.
//

#ifndef PMPC_OPTIONS_H
#define PMPC_OPTIONS_H

#include "../utilities/args.hxx"

namespace PMPC {

class Options {
public:
    /// Constructor
    Options();

    /// Parse
    void ParseCLI(int argc, char *argv[]);

    ///\name Parser and options
    /// @{
    args::ArgumentParser parser;
    args::HelpFlag help;
    args::Group ser_par;
    args::Flag serial;
    args::Flag parallel;
    args::Positional<std::string> config_file;
    ///@}
};
}


#endif //PMPC_OPTIONS_H
