#ifndef PMPC_PARALLEL_H
#define PMPC_PARALLEL_H

#include "../mpi/MPICom.h"
#include "Options.h"

namespace PMPC {
namespace parallel {

/**
 * \fn main
 *
 * \brief Main parallel execution of the PMPC ray tracer
 */
void main(PMPC::Options &options);

/**
 * \fn master
 *
 * \brief Main program for the master process
 */
void master(MPICom::MPI &mpi, PMPC::Options &options);

/**
 * \fn slave
 *
 * \brief Main program for the slave processes
 */
void slave(MPICom::MPI &mpi);

} // namespace parallel
} // namespace PMPC


#endif //PMPC_PARALLEL_H
