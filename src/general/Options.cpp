//
// Created by leroy on 7/3/17.
//

#include "Options.h"

PMPC::Options::Options() :
    parser("PMPC: the Porous Media Properties Calculator."
           "This program computes radiative distribution functions for porous media."),
    help(parser, "help", "Display this help menu", {'h', "help"}),
    ser_par(parser, "Execution mode (select one)", args::Group::Validators::Xor),
    serial(ser_par, "serial", "Serial", {'s', "serial"}),
    parallel(ser_par, "parallel", "Parallel", {'p', "parallel"}),
    config_file(parser, "config_file", "Configuration file")
{}

void PMPC::Options::ParseCLI(int argc, char *argv[]) {
    parser.ParseCLI(argc, argv);
}