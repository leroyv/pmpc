#include "../dots/Medium.h"

int main(int argc, char *argv[]) {
	// Create a medium
	dots::Medium medium("dots", 0.82, "dirac", 0.0, 3.0);

	// Save it to RAW
	medium.exportToRaw("report/dots_082.raw", 256);

	return 0;
}