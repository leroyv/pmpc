#include "parallel.h"

#include <libconfig.h++>
#include <boost/date_time.hpp>

#include "../utilities/filesystem/path.h"
#include "../dots/MonteCarlo.h"
#include "../dots/PostProcessor.h"
#include "../utilities/log.h"

using namespace std;
using namespace boost::posix_time;

void PMPC::parallel::main(PMPC::Options &options) {

    MPICom::MPI mpi;

    if (mpi.rank() == 0) {
        PMPC::parallel::master(mpi, options);
    } else {
        PMPC::parallel::slave(mpi);
    }

}

void PMPC::parallel::master(MPICom::MPI &mpi, PMPC::Options &options) {

    int *p_buffer_int = nullptr;
    double *p_buffer_double = nullptr;

    // Simulation parameters declaration
    double porosity, stdDeviation, sPlusMax, stdDevCriterion;
    int resAbscissa, resAngle, nGroups, nShotsPerInstance, nIterationsCheck,
        nMaxIterations, probLawIndex;
    string probLaw;
    filesystem::path reportPath;
    Log *pLogfile = nullptr;
    vector<double> alphaDiffList, alphaSpecList;

    try {

        // Read configuration file
        cout << "[D] Reading configuration file" << endl;
        filesystem::path pathToConfigFile(args::get(options.config_file));
        libconfig::Config myConfig;
        myConfig.readFile(pathToConfigFile.str().c_str());

        // Open log file
        reportPath = (const string &) (myConfig.lookup("solver.reportPath"));
        cout << "[D] Creating data output directory" << endl;
        // If not existing, the output directory is created
        reportPath.as_absolute().mkdirp();

        cout << "[D] Opening log file" << endl;
        pLogfile = new Log(reportPath.str() + "/computeGe.log");

        // Variable initialization
        (*pLogfile).write("[D] Variable initialization");
        porosity = static_cast<double>(myConfig.lookup("medium.porosity"));
        probLaw = (const string &) (myConfig.lookup("medium.probLaw"));
        probLawIndex = dots::probLawStr2Index(probLaw);
        stdDeviation = static_cast<double>(myConfig.lookup("medium.stdDeviation"));
        sPlusMax = static_cast<double>(myConfig.lookup("medium.sPlusMax"));

        resAbscissa = static_cast<int>(myConfig.lookup("solver.resAbscissa"));
        resAngle = static_cast<int>(myConfig.lookup("solver.resAngle"));
        nGroups = static_cast<int>(myConfig.lookup("solver.nGroups"));
        nShotsPerInstance = static_cast<int>(myConfig.lookup("solver.nShotsPerInstance"));
        nIterationsCheck = static_cast<int>(static_cast<double>(myConfig.lookup("solver.nIterationsCheck")));
        nMaxIterations = static_cast<int>(static_cast<double>(myConfig.lookup("solver.nMaxIterations")));
        stdDevCriterion = static_cast<double>(myConfig.lookup("solver.stdDevCriterion"));

        libconfig::Setting &alphaDiffSetting = myConfig.lookup("postprocess.alphaDiff");
        libconfig::Setting &alphaSpecSetting = myConfig.lookup("postprocess.alphaSpec");
        alphaDiffList.resize(alphaDiffSetting.getLength());
        alphaSpecList.resize(alphaSpecSetting.getLength());
        for (int i = 0; i != alphaDiffSetting.getLength(); ++i)
            alphaDiffList[i] = static_cast<double>(alphaDiffSetting[i]);
        for (int i = 0; i != alphaSpecSetting.getLength(); ++i)
            alphaSpecList[i] = static_cast<double>(alphaSpecSetting[i]);

    } catch (const libconfig::SettingTypeException &myException) {
        cout << "Error: wrong parameter type [" << myException.getPath() << "]" << endl;
    } catch (const libconfig::SettingNotFoundException &myException) {
        cout << "Error: could not find parameter [" << myException.getPath() << "]" << endl;
    } catch (const libconfig::FileIOException &myException) {
        cout << "Error: configuration file could not be read" << endl;
    }

    // Send simulation parameters to slave processes
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Bcast(&porosity, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&stdDeviation, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&sPlusMax, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&resAbscissa, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&resAngle, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&nGroups, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&nShotsPerInstance, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&nIterationsCheck, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&nMaxIterations, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&probLawIndex, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&stdDevCriterion, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Barrier(MPI_COMM_WORLD);

    // Initialization of communication buffers
    int resultsSize = (resAbscissa + resAngle) * (2 * nGroups + 1) + resAngle;
    p_buffer_double = new double[resultsSize];
    for (int i = 0; i != resultsSize; ++i) p_buffer_double[i] = 0;

    p_buffer_int = new int[2];
    p_buffer_int[0] = 0;
    p_buffer_int[1] = 0;

    // Gather & process data, monitor computations
    // Prepare computation
    dots::PostProcessor myPostProcessor(resAbscissa, resAngle, nGroups, sPlusMax);
    vector<double> stdDev(4);
    int nActiveProcesses = mpi.world_size() - 1;
    int rank_src = 0;
    MPI_Status status;
    ptime startTime = second_clock::local_time();
    (*pLogfile).write("[I] Starting iterating");

    // Iterate
    while (true) {
        // Receive data from slave

        // Get state info and source process ID
        MPI_Probe(
            MPI_ANY_SOURCE,
            MPICom::MESSAGETYPE_DATA_INT.tag,
            MPI_COMM_WORLD,
            &status);
        rank_src = status.MPI_SOURCE;

        // Receive number of iterations
        mpi.recv(MPICom::MESSAGETYPE_DATA_INT, rank_src, p_buffer_int, 2);

        // Receive solver data
        mpi.recv(MPICom::MESSAGETYPE_DATA_DOUBLE, rank_src, p_buffer_double, resultsSize);

        // Aggregate data
        myPostProcessor.gather(p_buffer_double, p_buffer_int);
        stdDev = myPostProcessor.getGroupStdDeviation();

        // Check for stop condition
        (*pLogfile).write(
            "[I] " + stringify(myPostProcessor.getNIterations()) + " iterations -- standard deviation: " +
            stringify(stdDev));

        if (myPostProcessor.getNIterations() >= nMaxIterations) {

            (*pLogfile).write("[W] Maximum number of iterations reached, sending STOP order to process "
                              + stringify(rank_src));

            *p_buffer_int = static_cast<int>(MPICom::Order::STOP);
            mpi.send(MPICom::MESSAGETYPE_ORDER, rank_src, p_buffer_int, 1);
            nActiveProcesses -= 1;

        } else if (   stdDev[0] < stdDevCriterion
                      && stdDev[1] < stdDevCriterion
                      && stdDev[2] < stdDevCriterion
                      && stdDev[3] < stdDevCriterion) {

            (*pLogfile).write("[I] Reached stop condition, sending STOP order to process "
                              + stringify(rank_src));

            *p_buffer_int = static_cast<int>(MPICom::Order::STOP);
            mpi.send(MPICom::MESSAGETYPE_ORDER, rank_src, p_buffer_int, 1);
            nActiveProcesses -= 1;

        } else {

            *p_buffer_int = static_cast<int>(MPICom::Order::COMPUTE);
            mpi.send(MPICom::MESSAGETYPE_ORDER, rank_src, p_buffer_int, 1);

        }

        if (nActiveProcesses == 0) break; // Stop if no more slave process is active

    } // while (true)

    // Post-processing
    (*pLogfile).write("[I] Post-processing");

    myPostProcessor.postProcess(alphaDiffList, alphaSpecList);

    // Write results to disk
    ptime endTime = second_clock::local_time();
    (*pLogfile).write("[I] Ending computation");
    (*pLogfile).write("[I] Total duration: " + stringify(to_simple_string(endTime - startTime)));
    (*pLogfile).write("[I] B+ = " + stringify(myPostProcessor.getBPlus()));
    (*pLogfile).write("[I] gDiff = " + stringify(myPostProcessor.getGDiff()));

    myPostProcessor.report(reportPath.str());
    delete pLogfile;
    delete[] p_buffer_double;
    delete[] p_buffer_int;

}

void PMPC::parallel::slave(MPICom::MPI &mpi) {

    int *p_buffer_int = nullptr;
    double *p_buffer_double = nullptr;

    // Simulation parameters declaration
    double porosity, stdDeviation, sPlusMax, stdDevCriterion;
    int resAbscissa, resAngle, nGroups, nShotsPerInstance, nIterationsCheck,
        nMaxIterations, probLawIndex;

    // Receive parameters from process
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Bcast(&porosity, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&stdDeviation, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&sPlusMax, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&resAbscissa, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&resAngle, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&nGroups, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&nShotsPerInstance, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&nIterationsCheck, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&nMaxIterations, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&probLawIndex, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&stdDevCriterion, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Barrier(MPI_COMM_WORLD);

    // Initialization of communication buffers
    int resultsSize = (resAbscissa + resAngle) * (2 * nGroups + 1) + resAngle;
    p_buffer_double = new double[resultsSize];
    for (int i = 0; i != resultsSize; ++i) p_buffer_double[i] = 0;

    p_buffer_int = new int[2];
    p_buffer_int[0] = 0;
    p_buffer_int[1] = 0;

    // Prepare computation
    int order = 0;
    int rank_dst = 0;

    dots::MonteCarlo mySolver;
    mySolver.initMedium("dots",
                        porosity,
                        dots::probLawIndex2Str(probLawIndex),
                        stdDeviation,
                        sPlusMax);
    mySolver.initSolver(resAbscissa,
                        resAngle,
                        nGroups,
                        nShotsPerInstance);
    mySolver.initRandomGenerator((int) time(0) + mpi.rank());

    // Iterate
    while (true) {
        // Compute
        mySolver.iterate(nIterationsCheck);

        // Serialize results
        mySolver.getResultsSerialized(p_buffer_double, p_buffer_int);

        // Send number of iterations and failures
        mpi.send(MPICom::MESSAGETYPE_DATA_INT, rank_dst, p_buffer_int, 2);

        // Send results
        mpi.send(MPICom::MESSAGETYPE_DATA_DOUBLE, rank_dst, p_buffer_double, resultsSize);

        // Receive order after master has checked for stop condition
        mpi.recv(MPICom::MESSAGETYPE_ORDER, rank_dst, &order, 1);

        if (order == static_cast<unsigned int>(MPICom::Order::STOP)) {
            break;
        }

        // Purge solver before starting next iteration cycle
        mySolver.purge();
    }

    delete[] p_buffer_double;
    delete[] p_buffer_int;

}
