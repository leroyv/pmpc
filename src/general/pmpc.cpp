#include <iostream>
#include "Options.h"
#include "parallel.h"
#include "../utilities/filesystem/path.h"

int main(int argc, char *argv[]) {

    PMPC::Options options;

    try {
        options.ParseCLI(argc, argv);
    }
    catch (args::Help) {
        std::cout << options.parser;
        return 0;
    }
    catch (args::ParseError e) {
        std::cerr << e.what() << std::endl;
        std::cerr << options.parser;
        return 1;
    }

    if (options.serial) {
        std::cout << "Serial mode activated" << std::endl;
        filesystem::path my_path(args::get(options.config_file));
        std::cout << my_path.str() << std::endl;
    }

    if (options.parallel) {
        PMPC::parallel::main(options);
    }

    return 0;

}
