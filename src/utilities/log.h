/*
 * Basic logging class.
 */

#ifndef LOG_H
#define LOG_H

#include <string>
#include <iostream>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/date_time.hpp>
#include "convert.h"

class Log {

public:

  // Constructor and destructor
  Log(boost::filesystem::path filename);
  ~Log();

  // Accessors
  inline std::ofstream &getFile() { return m_stream; }

  // Other member functions
  void write(std::string const &logline);

private:

  boost::filesystem::ofstream m_stream;

};

// Implementation ----------------------------------------------------------------------

Log::Log(boost::filesystem::path filename) {
  m_stream.open(filename);
}

void Log::write(std::string const &logline) {
  std::string myString;
  boost::posix_time::ptime now = boost::posix_time::second_clock::local_time();
  myString = stringify(now.date()) + " " + stringify(now.time_of_day()) + " " + logline;

  std::cout << myString << std::endl;
  m_stream << myString << std::endl;
}

Log::~Log(){
  m_stream.close();
}





#endif
