/* 
 * File:   array3d.hpp
 * Author: leroyv
 *
 * Created on 12 septembre 2012, 13:35
 */

/*
 * A basic 3D array class. Elements with adjacent i indices are contiguous.
 */

#ifndef ARRAY3D_HPP
#define	ARRAY3D_HPP

#include <vector>

template <typename T = int>
class array3d {
public:
  // Constructors
  array3d() { reset(0,0,0); }
  array3d(int i, int j, int k) { reset(i,j,k); }
  
  // Accessors
  int getSizeX() const { return m_size[0]; } 
  int getSizeY() const { return m_size[1]; } 
  int getSizeZ() const { return m_size[2]; } 
  int getElement(int i, int j, int k) const { return m_data[i+m_size[0]*(j+m_size[1]*k)]; } 
  const std::vector<T> & getData() const { return m_data; } 
  
  // Setters
  void setElement(int i, int j, int k, const T &data) { m_data[i+m_size[0]*(j+m_size[1]*k)] = data; }
  void reset(int i = 0, int j = 0, int k = 0) { m_data.resize(i*j*k); 
                                                m_size[0] = i;
                                                m_size[1] = j;
                                                m_size[2] = k;
                                                m_data.clear(); }
  void clear() { m_data.clear(); }

private:
  // Attributes
  std::vector<T> m_data;
  int m_size[3];
  
};

#endif	/* ARRAY3D_HPP */

