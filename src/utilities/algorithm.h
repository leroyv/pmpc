#ifndef ALGORITHM_H
#define ALGORITHM_H

#include <map>

template<typename T1, typename T2>
bool isValueInMapKeys(T1 const &toMatch, T2 const &myMap) {
	/*
	 * Returns true if toMatch is one of the keys of myMap, false otherwise. myMap is supposed to be a map of bimap.
	 */

	for (typename T2::const_iterator it = myMap.begin(); it != myMap.end(); ++it) {
		if (it->first == toMatch)
			return true;
	}

	return false;
}


template<typename T1, typename T2>
bool mapKeysIncluded(T1 const &map1, T2 const &map2) {
	/*
	 * Returns true if the key set of map1 is included in that of map2, false otherwise.
	 */

	for (typename T1::const_iterator it = map1.begin(); it != map1.end(); ++it) {
		if (!isValueInMapKeys(it->first, map2))
			return false;
	}

	return true;
}


template<typename T1, typename T2>
bool mapKeysMatch(T1 const &map1, T2 const &map2) {
	/*
	 * Returns true if map1 and map2 have identical key sets, false otherwise.
	 */

	return mapKeysIncluded(map1, map2) && mapKeysIncluded(map2, map1);
}


template<typename T1, typename T2>
T1 mapTakeValueOrSupersede(T2 const &key, std::map<T2, T1> const &baseMap, std::map<T2, T1> const &supersedeMap) {
	/*
	 * Returns the value associated the "key" key of the supersededMap argument, and that of baseMap otherwise.
	 */

	if (isValueInMapKeys(key, supersedeMap)) {
		return supersedeMap[key];
	} else {
		return baseMap[key];
	}
}


template<typename T1, typename T2>
std::map<T1, T2> mapMergeSupersede(std::map<T1, T2> const &baseMap, std::map<T1, T2> const &supersedeMap) {
	/*
	 * Returns a map containing all the values in baseMap, overwritten by those in supersedeMap.
	 */
	std::map<T1, T2> returnMap = baseMap;

	for (typename std::map<T1, T2>::const_iterator it = supersedeMap.begin(); it != supersedeMap.end(); ++it) {
		returnMap[it->first] = it->second;
	}

	return returnMap;
}

#endif
