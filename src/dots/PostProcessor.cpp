/** dots::PostProcessor class implementation
  */

#include "PostProcessor.h"

#include <iostream>
#include <fstream>
#include <cmath>
#include <map>
#include "../msm/msm.h"

using std::vector;
using std::cout;
using std::endl;
using std::sqrt;
using std::map;
using std::string;
using msm::Function;
using msm::misc::sqr;

namespace dots {

	// Constructor
	PostProcessor::PostProcessor(int abscissaResolution, int angleResolution, int nGroups, double maxAbscissa) {
		(*this).init(abscissaResolution, angleResolution, nGroups, maxAbscissa);
	}

// -----------------------------------------------------------------------------

	// Initialization
	void PostProcessor::init(int abscissaResolution, int angleResolution, int nGroups, double maxAbscissa) {
		m_abscissaResolution = abscissaResolution;
		m_angleResolution = angleResolution;
		m_nGroups = nGroups;
		m_maxAbscissa = maxAbscissa;

		// Resize and purge result containers
		m_resultsExtinction.resize(m_nGroups);
		m_resultsAbsorption.resize(m_nGroups);
		m_resultsMui.resize(m_nGroups);
		m_resultsMus.resize(m_nGroups);

		for (int i = 0; i != m_nGroups; ++i) {
			m_resultsExtinction[i].assign(m_abscissaResolution, 0);
			m_resultsAbsorption[i].assign(m_abscissaResolution, 0);
			m_resultsMui[i].assign(m_angleResolution, 0);
			m_resultsMus[i].assign(m_angleResolution, 0);
		}

		// Resize and purge abscissa arrays
		m_abscissaList.resize(m_abscissaResolution);
		m_muiList.resize(m_angleResolution);
		m_musList.resize(m_angleResolution);

		for (int i = 0; i != m_abscissaResolution; ++i) {
			m_abscissaList[i] = i * 0.4 / (m_abscissaResolution - 1);
		}

		for (int i = 0; i != m_angleResolution; ++i) {
			m_muiList[i] = (double) i / (m_angleResolution - 1);
			m_musList[i] = -1 + 2 * (double) i / (m_angleResolution - 1);
		}

		m_nIterations = 0;
		m_nFail = 0;
	}

// -----------------------------------------------------------------------------

	// Gather data
	void PostProcessor::gather(double *results, int *solverState) {
		/** results is an array of doubles of size
		  *   2 * m_abscissaResolution * m_nGroups
		  * + 2 * m_angleResolution * m_nGroups
		  * + m_abscissaResolution
		  * + 2 * m_angleResolution
		  * = (m_abscissaResolution + m_angleResolution)*(2*m_nGroups + 1) + m_angleResolution
		  *
		  * It contains:
		  * - m_resultsExtinction[0]
		  * - m_resultsExtinction[1]
		  * - ...
		  * - m_resultsExtinction[m_nGroups-1]
		  * - m_resultsAbsorption[0]
		  * - m_resultsAbsorption[1]
		  * - ...
		  * - m_resultsAbsorption[m_nGroups-1]
		  * - m_resultsMui[0]
		  * - m_resultsMui[1]
		  * - ...
		  * - m_resultsMui[m_nGroups-1]
		  * - m_resultsMus[0]
		  * - m_resultsMus[1]
		  * - ...
		  * - m_resultsMus[m_nGroups-1]
		  * - m_abscissaList
		  * - m_muiList
		  * - m_musList
		  *
		  * solverState is an array of size 2
		  * It contains:
		  * - m_nIterations
		  * - m_nFail
		  *
		  * WATCH OUT FOR SEGFAULT
		  */

		int offset = 0;

		for (int i = 0; i != m_nGroups; ++i) {
			for (int j = 0; j != m_abscissaResolution; ++j) {
				m_resultsExtinction[i][j] += results[i * m_abscissaResolution + j];
			}
		}

		offset += m_abscissaResolution * m_nGroups;

		for (int i = 0; i != m_nGroups; ++i) {
			for (int j = 0; j != m_abscissaResolution; ++j) {
				m_resultsAbsorption[i][j] += results[offset + i * m_abscissaResolution + j];
			}
		}

		offset += m_abscissaResolution * m_nGroups;

		for (int i = 0; i != m_nGroups; ++i) {
			for (int j = 0; j != m_angleResolution; ++j) {
				m_resultsMui[i][j] += results[offset + i * m_angleResolution + j];
			}
		}

		offset += m_angleResolution * m_nGroups;

		for (int i = 0; i != m_nGroups; ++i) {
			for (int j = 0; j != m_angleResolution; ++j) {
				m_resultsMus[i][j] += results[offset + i * m_angleResolution + j];
			}
		}

		offset += m_angleResolution * m_nGroups;

		// Suboptimal (the data is overwritten at each execution). However, performance is not critical.
		for (int i = 0; i != m_nGroups; ++i) {
			m_abscissaList[i] = results[offset + i];
		}

		offset += m_abscissaResolution;

		for (int i = 0; i != m_nGroups; ++i) {
			m_muiList[i] = results[offset + i];
		}

		offset += m_angleResolution;
		for (int i = 0; i != m_nGroups; ++i) {
			m_musList[i] = results[offset + i];
		}

		// End of suboptimal zone

		m_nIterations += solverState[0];
		m_nFail += solverState[1];

	}

	//void postProcess::gather(MonteCarlo const &solver) {}

// -----------------------------------------------------------------------------

	// Data processing
	void PostProcessor::postProcess(std::vector<double> const &alphaDiff, std::vector<double> const &alphaSpec) {
		/** Post-process results gathered with the gather() member function.
		  * alphaDiff: diffuse reflection coefficient
		  * alphaSpec: specular reflection coefficient
		  */

		m_alphaDiff = alphaDiff;
		m_alphaSpec = alphaSpec;

		// Abscissa list
		m_postProcAbscissaList.resize(m_abscissaResolution);
		for (int i = 0; i != m_abscissaResolution; ++i)
			m_postProcAbscissaList[i] = m_abscissaList[i] / 0.4 * m_maxAbscissa;

		// Cumulative extinction distribution function
		m_postProcGe.resize(m_abscissaResolution);
		double sumUnextinguishedRaysTotal = 0;
		for (int i = 0; i != m_nGroups; ++i) sumUnextinguishedRaysTotal += m_resultsExtinction[i][0];

		for (int i = 0; i != m_abscissaResolution; ++i) {
			double sumUnextinguishedRays = 0;
			for (int j = 0; j != m_nGroups; ++j) sumUnextinguishedRays += m_resultsExtinction[j][i];
			m_postProcGe[i] = 1. - sumUnextinguishedRays / sumUnextinguishedRaysTotal;
		}

		// Cumulative specular absorption probability
		m_postProcPaSpec.resize(m_alphaSpec.size());

		for (int i = 0; i != m_alphaSpec.size(); ++i) {
			m_postProcPaSpec[i].resize(m_abscissaResolution);
			double sumUnabsorbedRaysTotal = 0;
			for (int j = 0; j != m_nGroups; ++j) sumUnabsorbedRaysTotal += m_resultsAbsorption[j][0];

			for (int j = 0; j != m_abscissaResolution; ++j) {
				double sumUnabsorbedRays = 0;
				for (int k = 0; k != m_nGroups; ++k) sumUnabsorbedRays += m_resultsAbsorption[k][j];
				m_postProcPaSpec[i][j] =
					1.5 * m_alphaSpec[i] * (sumUnabsorbedRaysTotal - sumUnabsorbedRays) / sumUnextinguishedRaysTotal;
			}
		}

		// Cumulative diffuse absorption probability
		m_postProcPaDiff.resize(m_alphaDiff.size());

		for (int i = 0; i != m_alphaDiff.size(); ++i) {
			m_postProcPaDiff[i].resize(m_abscissaResolution);
			for (int j = 0; j != m_abscissaResolution; ++j) m_postProcPaDiff[i][j] = m_alphaDiff[i] * m_postProcGe[j];
		}

		// Gi
		m_postProcGi.resize(m_angleResolution);
		double sumMuiTotal = 0;
		for (int i = 0; i != m_nGroups; ++i) sumMuiTotal += m_resultsMui[i][0];

		for (int i = 0; i != m_angleResolution; ++i) {
			double sumMui = 0;
			for (int j = 0; j != m_nGroups; ++j) sumMui += m_resultsMui[j][i];
			m_postProcGi[i] = 1. - sumMui / sumMuiTotal;
		}

		// PnuSpec
		m_postProcPnuSpec.resize(m_alphaSpec.size());

		for (int i = 0; i != m_alphaSpec.size(); ++i) {
			m_postProcPnuSpec[i].resize(m_muiList.size());
			Function Gi(m_muiList, m_postProcGi);
			Function Fi = Gi.diff();
			Function identity = Gi;
			identity.setIdentity();
			// A point (1,1) must be added to the definition of Fi to simplify the computation of PnuSpec
			// (arbitrary value, the corresponding point will be wrong anyway)
			vector<double> FiOrdinate = Fi.getOrdinate();
			FiOrdinate.push_back(1);
			Fi.setData(m_muiList, FiOrdinate);
			Function myFunction = 1.5 * m_alphaSpec[i] * identity;
			double magicIntegral = (Fi * (1 - myFunction)).integral();
			for (int j = 0; j != m_postProcPnuSpec[i].size(); ++j) {
				m_postProcPnuSpec[i][j] =
					(Fi(sqrt(0.5 * (1 - m_musList[j]))) * (1 - 1.5 * m_alphaSpec[i] * sqrt(0.5 * (1 - m_musList[j])))) /
					(4 * sqrt(0.5 * (1 - m_musList[j])) * magicIntegral);
			}
		}

		// Gs
		m_postProcGs.resize(m_angleResolution);
		double sumMusTotal = 0;
		for (int i = 0; i != m_nGroups; ++i) sumMusTotal += m_resultsMus[i][0];

		for (int i = 0; i != m_angleResolution; ++i) {
			double sumMus = 0;
			for (int j = 0; j != m_nGroups; ++j) sumMus += m_resultsMus[j][i];
			m_postProcGs[i] = 1. - sumMus / sumMusTotal;
		}

		// PnuDiff
		Function PnuDiff(m_musList, m_postProcGs);
		PnuDiff = PnuDiff.diff();
		m_postProcPnuDiff = PnuDiff.getOrdinate();

		// BPlus
		Function Ge(m_postProcAbscissaList, m_postProcGe);
		double a = -log(1 - Ge(Ge.getUpperBound())) / m_postProcAbscissaList[m_postProcAbscissaList.size() - 1];
		// This coefficient allows to extend an incomplete Ge by continuity (the uncomputed part being evaluated with an
		// exponential profile)
		m_postProcBPlus =
			1 / ((1 - Ge).integral() + exp(-a * m_postProcAbscissaList[m_postProcAbscissaList.size() - 1]) / a);

		// GDiff
		Function integrand, identity;
		vector<double> myVector = PnuDiff.getAbscissa();
		for (int i = 0; i != myVector.size() - 1; ++i) {
			myVector[i] = myVector[i] * PnuDiff[i][1];
		}
		integrand.setData(PnuDiff.getAbscissa(), myVector);

		m_postProcGDiff = 0.5 * integrand.integral();

	}

	void PostProcessor::postProcess(double alphaDiff, double alphaSpec) {
		(*this).postProcess(vector<double>(1, alphaDiff), vector<double>(1, alphaSpec));
	}

// -----------------------------------------------------------------------------

	// Standard deviation
	std::vector<double> PostProcessor::getGroupStdDeviation() {

		// This vector contains the final results (0: extinction, 1: absorption, 2: mui, 3: mus)
		vector<double> result(4);
		vector<vector<double> > *pData = NULL;

		for (int i = 0; i != 4; ++i) {
			switch (i) {
				case 0: // Extinction
					pData = &m_resultsExtinction;
					break;

				case 1: // Absorption
					pData = &m_resultsAbsorption;
					break;

				case 2: // Mui
					pData = &m_resultsMui;
					break;

				case 3: // Mus
					pData = &m_resultsMus;
					break;

				default:
					break;
			} // switch i

			vector<double> mean((*pData).size());
			vector<double> stdDeviation((*pData).size());

			for (int j = 0; j != (*pData).size(); ++j) { // Mean
				double sum = 0;
				for (int k = 0; k != m_nGroups; ++k) sum += (*pData)[k][j];
				mean[j] = sum / (*pData).size();
			}

			for (int j = 0; j != (*pData).size(); ++j) { // Standard deviation
				double sum = 0;
				for (int k = 0; k != m_nGroups; ++k) sum += sqr((*pData)[k][j] - mean[j]);
				stdDeviation[j] = (sqrt(sum / (*pData).size())) / mean[j];
			}

			double sum = 0;
			for (int j = 0; j != stdDeviation.size(); ++j) sum += stdDeviation[j];
			result[i] = sum / (double) stdDeviation.size();
		}

		return result;
	}

// -----------------------------------------------------------------------------

	// Write results to disk
	void PostProcessor::report(filesystem::path const &pathToSaveDir) {
		/*
		 * Write results to disk
		 * TODO: if pathToSaveDir == "", output results to standard output
		 */

		map<string, string> fileNameMap;

		fileNameMap["ge"] = "ge.csv";
		fileNameMap["paDiff"] = "paDiff.csv";
		fileNameMap["paSpec"] = "paSpec.csv";
		fileNameMap["gi"] = "gi.csv";
		fileNameMap["gs"] = "gs.csv";
		fileNameMap["pnuDiff"] = "pnuDiff.csv";
		fileNameMap["pnuSpec"] = "pnuSpec.csv";
		fileNameMap["misc"] = "misc.csv";


		if (!pathToSaveDir.exists()) { // Create output dir if it doesn't exist
			pathToSaveDir.mkdirp();
		}

		std::fstream outputFile;

		outputFile.open((pathToSaveDir / fileNameMap["ge"]).str(), std::ios::out | std::ios::trunc);
		outputFile << "\"sPlus\", \"Gext\"" << endl;
		for (int i = 0; i != m_postProcAbscissaList.size(); ++i) {
			outputFile << m_postProcAbscissaList[i] << "," << m_postProcGe[i] << endl;
		}
		outputFile.close();


		outputFile.open((pathToSaveDir / fileNameMap["paDiff"]).str(), std::ios::out | std::ios::trunc);
		outputFile << "\"sPlus\"";
		for (int i = 0; i != m_alphaDiff.size(); ++i) outputFile << ",\"PaDiff_alphaDiff_" << m_alphaDiff[i] << "\"";
		outputFile << endl;

		for (int i = 0; i != m_postProcAbscissaList.size(); ++i) {
			outputFile << m_postProcAbscissaList[i];
			for (int j = 0; j != m_alphaDiff.size(); ++j) outputFile << "," << m_postProcPaDiff[j][i];
			outputFile << endl;
		}
		outputFile.close();


		outputFile.open((pathToSaveDir / fileNameMap["paSpec"]).str(), std::ios::out | std::ios::trunc);
		outputFile << "\"sPlus\"";
		for (int i = 0; i != m_alphaDiff.size(); ++i) outputFile << ",\"PaSpec_alphaSpec_" << m_alphaSpec[i] << "\"";
		outputFile << endl;

		for (int i = 0; i != m_postProcAbscissaList.size(); ++i) {
			outputFile << m_postProcAbscissaList[i];
			for (int j = 0; j != m_alphaSpec.size(); ++j) outputFile << "," << m_postProcPaSpec[j][i];
			outputFile << endl;
		}
		outputFile.close();


		outputFile.open((pathToSaveDir / fileNameMap["gi"]).str(), std::ios::out | std::ios::trunc);
		outputFile << "\"mui\", \"Gi\"" << endl;
		for (int i = 0; i != m_muiList.size(); ++i) {
			outputFile << m_muiList[i] << "," << m_postProcGi[i] << endl;
		}
		outputFile.close();


		outputFile.open((pathToSaveDir / fileNameMap["gs"]).str(), std::ios::out | std::ios::trunc);
		outputFile << "\"mus\", \"Gs\"" << endl;
		for (int i = 0; i != m_musList.size(); ++i) {
			outputFile << m_musList[i] << "," << m_postProcGs[i] << endl;
		}
		outputFile.close();


		outputFile.open((pathToSaveDir / fileNameMap["pnuDiff"]).str(), std::ios::out | std::ios::trunc);
		outputFile << "\"mus\", \"PnuDiff\"" << endl;
		for (int i = 0; i != m_musList.size() - 1; ++i) {
			outputFile << m_musList[i] << "," << m_postProcPnuDiff[i] << endl;
		}
		outputFile.close();


		outputFile.open((pathToSaveDir / fileNameMap["pnuSpec"]).str(), std::ios::out | std::ios::trunc);
		outputFile << "\"mus\"";
		for (int i = 0; i != m_alphaDiff.size(); ++i) outputFile << ",\"PnuSpec_alphaSpec_" << m_alphaSpec[i] << "\"";
		outputFile << endl;

		for (int i = 0; i != m_muiList.size(); ++i) {
			outputFile << m_musList[i];
			for (int j = 0; j != m_alphaSpec.size(); ++j) outputFile << "," << m_postProcPnuSpec[j][i];
			outputFile << endl;
		}
		outputFile.close();

		outputFile.open((pathToSaveDir / fileNameMap["misc"]).str(), std::ios::out | std::ios::trunc);
		outputFile << "\"B+\"," << m_postProcBPlus << endl;
		outputFile << "\"gDiff\"," << m_postProcGDiff << endl;
	}
}
