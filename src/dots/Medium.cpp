#include "Medium.h"

#include <fstream>
#include <boost/algorithm/string.hpp>
#include "../utilities/convert.h"

using std::endl;
using std::min;
using std::max;
using msm::misc::sqr;
using msm::misc::cube;

namespace dots {
	Medium::Medium() : m_RandomGenerator((int) time(0)), m_mediumTypeIndex(0), m_porosity(0.8), m_probLawIndex(2),
	                   m_stdDeviation(0.0), m_maxAbscissa(5.0) {
		(*this).clear();
	}

	Medium::Medium(int mediumTypeIndex, double porosity, int probLawIndex, double stdDeviation, double maxAbscissa)
		: m_RandomGenerator((int) time(0)) {
		(*this).generate(mediumTypeIndex, porosity, probLawIndex, stdDeviation, maxAbscissa);
	}


	Medium::Medium(std::string mediumTypeStr, double porosity, std::string probLawStr, double stdDeviation,
	               double maxAbscissa) : m_RandomGenerator((int) time(0)) {
		(*this).generate(mediumTypeStr, porosity, probLawStr, stdDeviation, maxAbscissa);
	}


	void Medium::clear() {
		m_SphereList.clear();
		(*this).voxelClear();
	}


	void Medium::voxelClear() {
		m_voxelMedium.resize(boost::extents[0][0][0]);
		m_voxelMediumResolution = 0;
	}


	void Medium::save(filesystem::path const &pathToSaveFile) {
		std::fstream saveFile(pathToSaveFile.str(), std::ios::out | std::ios::trunc);

		saveFile << "# Medium dump file. File format: 0.1." << endl;

		saveFile << m_mediumTypeIndex << endl;
		saveFile << m_porosity << endl;
		saveFile << m_probLawIndex << endl;
		saveFile << m_stdDeviation << endl;
		saveFile << m_maxAbscissa << endl;

		for (SphereList::const_iterator it = m_SphereList.begin(); it != m_SphereList.end(); ++it) {
			saveFile << (*it) << endl;
		}

		saveFile.close();
	}


	void Medium::load(filesystem::path const &pathToLoadFile) {
		(*this).clear();

		std::ifstream loadFile(pathToLoadFile.str(), std::ios::in);
		std::string tempString;

		// On saute la première ligne, normalement constituée d'un commentaire
		getline(loadFile, tempString);

		// Récupération des paramètres du milieu
		getline(loadFile, tempString);
		boost::algorithm::trim(tempString); // Supression des espaces surnuméraires
		m_mediumTypeIndex = convertTo<int>(tempString);

		getline(loadFile, tempString);
		boost::algorithm::trim(tempString);
		m_porosity = convertTo<double>(tempString);

		getline(loadFile, tempString);
		boost::algorithm::trim(tempString);
		m_probLawIndex = convertTo<int>(tempString);

		getline(loadFile, tempString);
		boost::algorithm::trim(tempString);
		m_stdDeviation = convertTo<double>(tempString);

		getline(loadFile, tempString);
		boost::algorithm::trim(tempString);
		m_maxAbscissa = convertTo<double>(tempString);

		// Récupération de la liste de sphères
		while (getline(loadFile, tempString)) {
			boost::algorithm::trim(tempString);
			(*this).addSphere(tempString);
		}

		loadFile.close();
	}


	void Medium::exportToRaw(filesystem::path const &pathToSaveFile, int resolution) {
		boost::multi_array<char, 3> array3D(boost::extents[resolution][resolution][resolution]);

		int inSphereIndex, outSphereIndex;

		if (m_mediumTypeIndex == 0) { // DOTS case
			inSphereIndex = 0;
			outSphereIndex = 127;
		} else { // DOOS case
			inSphereIndex = 127;
			outSphereIndex = 0;
		}

		for (int i = 0; i != resolution; ++i)
			for (int j = 0; j != resolution; ++j)
				for (int k = 0; k != resolution; ++k)
					array3D[i][j][k] = outSphereIndex; // Initialisation


		msm::geometry3d::Point currentPoint(0, 0, 0);
		double minX, maxX, minY, maxY, minZ, maxZ;
		int mini, maxi, minj, maxj, mink, maxk;

		for (dots::SphereList::const_iterator it = m_SphereList.begin(); it != m_SphereList.end(); ++it) {
			minX = max(0., (*it).getCenter().getX() - (*it).getRadius());
			maxX = min(1., (*it).getCenter().getX() + (*it).getRadius());
			minY = max(0., (*it).getCenter().getY() - (*it).getRadius());
			maxY = min(1., (*it).getCenter().getY() + (*it).getRadius());
			minZ = max(0., (*it).getCenter().getZ() - (*it).getRadius());
			maxZ = min(1., (*it).getCenter().getZ() + (*it).getRadius());

			mini = minX * resolution;
			maxi = min((double) resolution - 1, maxX * resolution + 1);
			minj = minY * resolution;
			maxj = min((double) resolution - 1, maxY * resolution + 1);
			mink = minZ * resolution;
			maxk = min((double) resolution - 1, maxZ * resolution + 1);

			for (int i = mini; i <= maxi; ++i) {
				for (int j = minj; j <= maxj; ++j) {
					for (int k = mink; k <= maxk; ++k) {
						currentPoint.setCoordinates((i + 0.5) / resolution, (j + 0.5) / resolution,
						                            (k + 0.5) / resolution);
						if (currentPoint.distance((*it).getCenter()) < (*it).getRadius()) {
							array3D[i][j][k] = inSphereIndex;
						}
					}
				}
			}
		}


		// RAW export
		std::fstream outputFile(pathToSaveFile.str(), std::ios::out | std::ios::binary);
		for (int i = 0; i != resolution; ++i)
			for (int j = 0; j != resolution; ++j)
				for (int k = 0; k != resolution; ++k)
					outputFile << array3D[i][j][k];
	}


	void Medium::init(int mediumTypeIndex, double porosity, int problawIndex, double stdDeviation, double maxAbscissa) {
		m_mediumTypeIndex = mediumTypeIndex;
		m_porosity = porosity;
		m_probLawIndex = problawIndex;
		m_stdDeviation = stdDeviation;
		m_maxAbscissa = maxAbscissa;
	}


	void
	Medium::init(std::string const &mediumTypeStr, double porosity, std::string const &probLawStr, double stdDeviation,
	             double maxAbscissa) {
		(*this).init(mediumTypeStr2Index(mediumTypeStr), porosity, probLawStr2Index(probLawStr), stdDeviation,
		             maxAbscissa);
	}


	void Medium::generate() {
		(*this).clear(); // Cleanup medium
		m_SphereList.generate(m_probLawIndex, (*this).getAverageRadius(), m_stdDeviation, (*this).getNSpheresMath(),
		                      m_RandomGenerator);
	}


	void
	Medium::generate(int mediumTypeIndex, double porosity, int problawIndex, double stdDeviation, double maxAbscissa) {
		(*this).init(mediumTypeIndex, porosity, problawIndex, stdDeviation, maxAbscissa);
		(*this).generate();
	}


	void Medium::generate(std::string const &mediumTypeStr, double porosity, std::string const &probLawStr,
	                      double stdDeviation, double maxAbscissa) {
		(*this).init(mediumTypeStr, porosity, probLawStr, stdDeviation, maxAbscissa);
		(*this).generate();
	}


	void Medium::addSphere(double const x, double const y, double const z, double const r) {
		msm::geometry3d::Sphere mySphere(x, y, z, r);
		(*this).addSphere(mySphere);
	}


	void Medium::addSphere(std::string const &sphereStr) {
		msm::geometry3d::Sphere mySphere(sphereStr);
		(*this).addSphere(mySphere);
	}


	void Medium::trim(double newMaxAbscissa) {
		/*
		 * Reduces maximum asbcissa while preserving the sphere list. Shrinking is performed with trimming of the
		 * artefacts at the edges (i.e. the number density of spheres remins homogeneous, even at the edges).
		 */
		if (newMaxAbscissa >= m_maxAbscissa)
			throw std::out_of_range("The new value of maxAbscissa must be lower than the current one");

		double scalingFactor = newMaxAbscissa / m_maxAbscissa;

		// Adjust coordinates of all spheres so that the bounding box has a vertex at (0, 0, 0)
		for (std::list<msm::geometry3d::Sphere>::iterator it = m_SphereList.begin(); it != m_SphereList.end(); ++it) {
			double x = ((*it).getCenter().getX() - 0.5) / scalingFactor + 0.5;
			double y = ((*it).getCenter().getY() - 0.5) / scalingFactor + 0.5;
			double z = ((*it).getCenter().getZ() - 0.5) / scalingFactor + 0.5;
			double r = (*it).getRadius() / scalingFactor;
			(*it).setAll(x, y, z, r);
		}

		// Remove extra spheres
		std::list<msm::geometry3d::Sphere>::iterator it = m_SphereList.begin();
		bool hasToBeRemoved;
		while (it != m_SphereList.end()) {

			msm::geometry3d::Point cubeVertex1(0, 0, 0), cubeVertex2(1, 1, 1);
			msm::geometry3d::Point sphereCenter = (*it).getCenter();
			double sqrDistance = sqr((*it).getRadius());

			// Check if the sphere intersects the bounding box (or if it is inside). If not, trim it
			if (sphereCenter.getX() < cubeVertex1.getX()) sqrDistance -= sqr(sphereCenter.getX() - cubeVertex1.getX());
			else if (sphereCenter.getX() > cubeVertex2.getX())
				sqrDistance -= sqr(sphereCenter.getX() - cubeVertex2.getX());
			if (sphereCenter.getY() < cubeVertex1.getY()) sqrDistance -= sqr(sphereCenter.getY() - cubeVertex1.getY());
			else if (sphereCenter.getY() > cubeVertex2.getY())
				sqrDistance -= sqr(sphereCenter.getY() - cubeVertex2.getY());
			if (sphereCenter.getZ() < cubeVertex1.getZ()) sqrDistance -= sqr(sphereCenter.getZ() - cubeVertex1.getZ());
			else if (sphereCenter.getZ() > cubeVertex2.getZ())
				sqrDistance -= sqr(sphereCenter.getZ() - cubeVertex2.getZ());

			hasToBeRemoved = (sqrDistance < 0);

			if (hasToBeRemoved) it = m_SphereList.erase(it);
			else ++it;
		}

		// Modify medium parameters
		m_maxAbscissa = newMaxAbscissa;

	}


	double Medium::getAPlus() const {
		switch (m_mediumTypeIndex) {
			case 0: // DOTS
				switch (m_probLawIndex) {
					case 0: // Uniform
						return (3. * (*this).getNPlus() * (1. + sqr(m_stdDeviation) / 3.) /
						        (exp((*this).getNPlus() * (1. + sqr(m_stdDeviation))) - 1.));
						break;
					case 1: // Gaussian
						return (3. * (*this).getNPlus() * (1. + sqr(m_stdDeviation)) /
						        (exp((*this).getNPlus() * (1. + 3. * sqr(m_stdDeviation))) - 1.));
						break;
					case 2: // Dirac
						return (3. * (*this).getNPlus() / (exp((*this).getNPlus()) - 1.));
						break;
				}
				break; // End DOTS

			case 1: // DOOS
				switch (m_probLawIndex) {
					case 0: // Uniform
						return (3. * (*this).getNPlus() * (1. + sqr(m_stdDeviation) / 3.));
						break;
					case 1: // Gaussian
						return (3. * (*this).getNPlus() * (1. + sqr(m_stdDeviation)));
						break;
					case 2: // Dirac
						return (3. * (*this).getNPlus());
						break;
				}
				break; // End DOOS
		}

		return 0;
	}


	double Medium::getAverageRadius() const {
		return (0.4 * (*this).getAPlus() / (4. * m_maxAbscissa));
	}


	double Medium::getNPlus() const {
		switch (m_mediumTypeIndex) {
			case 0: // DOTS
				switch (m_probLawIndex) {
					case 0: // Uniform
						return (-log(1. - m_porosity) / (1. + sqr(m_stdDeviation)));
						break;
					case 1: // Gaussian
						return (-log(1. - m_porosity) / (1. + 3. * sqr(m_stdDeviation)));
						break;
					case 2: // Dirac
						return (-log(1. - m_porosity));
						break;
				}
				break; // End DOTS

			case 1: // DOOS
				switch (m_probLawIndex) {
					case 0: // Uniform
						return (-log(m_porosity) / (1. + sqr(m_stdDeviation)));
						break;
					case 1: // Gaussian
						return (-log(m_porosity) / (1. + 3. * sqr(m_stdDeviation)));
						break;
					case 2: // Dirac
						return (-log(m_porosity));
						break;
				}
				break; // End DOOS
		}

		return 0;
	}


	int Medium::getNSpheresMath() const {
		return ((int) (3 * (*this).getNPlus() / (4 * M_PI * cube((*this).getAverageRadius()))));
	}


	int Medium::getNSpheres() const {
		return m_SphereList.size();
	}


	std::string Medium::getMediumTypeStr() const {
		return mediumTypeIndex2Str(m_mediumTypeIndex);
	}


	std::string Medium::getProbLawStr() const {
		return probLawIndex2Str(m_probLawIndex);
	}
} // namespace dots
