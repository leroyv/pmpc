/*
 * Implementation of class dots::MonteCarlo
 */

#include "MonteCarlo.h"
#include <ctime>
#include <map>

using std::vector;
using std::cout;
using std::endl;
using std::max;
using std::min;
using std::exp;
using std::log;
using msm::geometry3d::Vector;
using msm::geometry3d::Point;
using msm::geometry3d::Sphere;
using msm::misc::sqr;
using std::string;
using std::map;
using std::sqrt;
using msm::Function;


namespace dots {

	MonteCarlo::MonteCarlo() : m_RandomGenerator((int) time(0)) {
	}

// -----------------------------------------------------------------------------

	void MonteCarlo::initRandomGenerator(int seed) {
		m_RandomGenerator.RandomInit(seed);
	}

// -----------------------------------------------------------------------------

	void MonteCarlo::initMedium(std::string const &mediumTypeStr, double porosity, std::string const &probLawStr,
	                            double stdDeviation, double maxAbscissa) {
		m_Medium.init(mediumTypeStr, porosity, probLawStr, stdDeviation, maxAbscissa);
	}

// -----------------------------------------------------------------------------

	void MonteCarlo::initSolver(int abscissaResolution, int angleResolution, int nGroups, int nShotsPerMedium) {
		// Set computational parameters
		m_abscissaResolution = abscissaResolution;
		m_angleResolution = angleResolution;
		m_nGroups = nGroups;
		m_nShotsPerMedium = nShotsPerMedium;

		// Resize results containers
		(*this).reset();
	}

// -----------------------------------------------------------------------------

	void MonteCarlo::reset() {
		m_nIterations = 0;
		m_nFail = 0;

		// Resize results containers
		m_resultsAbsorption.resize(m_nGroups);
		m_resultsExtinction.resize(m_nGroups);
		m_resultsMui.resize(m_nGroups);
		m_resultsMus.resize(m_nGroups);

		// Purge results containers
		(*this).purge();

		// Resize abscissa lists
		m_abscissaList.resize(m_abscissaResolution);
		m_muiList.resize(m_angleResolution);
		m_musList.resize(m_angleResolution);

		// Redefine abscissa lists
		for (int i = 0; i != m_abscissaResolution; ++i) {
			m_abscissaList[i] = i * 0.4 / (m_abscissaResolution - 1);
		}

		for (int i = 0; i != m_angleResolution; ++i) {
			m_muiList[i] = (double) i / (m_angleResolution - 1);
			m_musList[i] = -1 + 2 * (double) i / (m_angleResolution - 1);
		}

		// Reinitialize the medium and its RNG
		m_Medium.clear();
		m_Medium.resetRandom();

	}

// -----------------------------------------------------------------------------

	void MonteCarlo::purge() {
		for (int i = 0; i != m_nGroups; ++i) {
			m_resultsAbsorption[i].assign(m_abscissaResolution, 0);
			m_resultsExtinction[i].assign(m_abscissaResolution, 0);
			m_resultsMui[i].assign(m_angleResolution, 0);
			m_resultsMus[i].assign(m_angleResolution, 0);
		}
		m_nIterations = 0;
		m_nFail = 0;
	}

// -----------------------------------------------------------------------------

	void MonteCarlo::iterate(int nIterations) {
		// Iterate nIterations times. Iterations are spread evenly between the statistical groups

		// Declarations
		const int N_SHOTS_PER_GROUP = nIterations / m_nGroups;
		const int N_MEDIA_PER_GROUP = N_SHOTS_PER_GROUP / m_nShotsPerMedium;
		// This sets a maximum number of tries per medium to find a point in the transparent phase
		const int N_MAX_TRIES_PER_MEDIUM = m_nShotsPerMedium * 100;
		int nTries = 0;

		Point origin, intersect1, intersect2, impactPoint, impactSphereCenter;
		Vector direction, reflection, localNormal;
		double mui, mus;
		// End of declarations

		for (int iGroup = 0; iGroup != m_nGroups; ++iGroup) {
			vector<double> &unextinguishedRaysCount = m_resultsExtinction[iGroup];
			vector<double> &unabsorbedRaysCount = m_resultsAbsorption[iGroup];
			vector<double> &muiCumulated = m_resultsMui[iGroup];
			vector<double> &musCumulated = m_resultsMus[iGroup];

			for (int iMedium = 0; iMedium != N_MEDIA_PER_GROUP; ++iMedium) {

				m_Medium.generate();
				nTries = 0;

				for (int iShots = 0; iShots != m_nShotsPerMedium; ++iShots) {
					randomOrigin(origin, m_RandomGenerator);

					// Determine firing location stochastically
					while (not(pointInTrans(origin, m_Medium)) && nTries != N_MAX_TRIES_PER_MEDIUM) {
						randomOrigin(origin, m_RandomGenerator);
						++nTries;
					}

					// Determine firing direction stochastically
					if (nTries != N_MAX_TRIES_PER_MEDIUM) {
						randomDirection(direction, m_RandomGenerator);

						// Determine extinction point (intersection between the line defined by origin and direction
						// and a sphere, which is the closest to origin, and which is not in a pore)
						double distance = 1e300; // Initialize distance between origin and impact point

						for (SphereList::const_iterator pSphere = (m_Medium.getSphereList()).begin();
						     pSphere != (m_Medium.getSphereList()).end(); ++pSphere) {

							if (lineSphereIntersection(origin, direction, (*pSphere), intersect1,
							                           intersect2)) { // If there exists intersections between current sphere and the line...
								if ((intersect1 - origin).scalar(direction) >
								    0) { // ... check that the intersection point is in the way pointed by the direction vector ...
									if (not pointInTrans(intersect1,
									                     m_Medium)) { // ... check that the first intersection is not in the transparent phase (if the function returns true, it means that the point is not on an opaque/transparent interface)...
										if (origin.distance(intersect1) <
										    distance) { // ... check that the distance between origin and the intersection point minimizes the distance...
											// If all these conditions are met, store point coordinates and the center of the associated sphere (used for local normal computation)
											impactPoint = intersect1;
											impactSphereCenter = (*pSphere).getCenter();
											distance = origin.distance(impactPoint);
										}
									}
								}

								if ((intersect2 - origin).scalar(direction) > 0) {
									if (not pointInTrans(intersect2, m_Medium)) {
										if (origin.distance(intersect2) < distance) {
											impactPoint = intersect2;
											impactSphereCenter = (*pSphere).getCenter();
											distance = origin.distance(impactPoint);
										}
									}
								}
							}

						}

						// Compute normal vector at the impact point (the normal is oriented towards the interior of the sphere)
						localNormal = impactSphereCenter - impactPoint;
						localNormal.normalize();

						bool impactOffBound = isOffBounds(impactPoint);

						if (!impactOffBound) { // If impact point is not off-bound, compute the diffuse reflection angle (specular reflection does not require a stochastic procedure)
							// Initialize the reflected vector
							reflection.setCoordinates(0, 0, 0);

							while (reflection.scalar(localNormal) <= 0) {
								randomDirection(reflection, m_RandomGenerator);
							} // The reflection vector must point towards the appropriate half-space

							mus = direction.scalar(reflection); // Both vectors are supposed to be normalized. WARNING: mus is the cosine of the scattering angle as defined by Tancrez (2004)

						}

						// Update the number of unextincted rays (rays which leave the bounding box are put in a common bin)
						for (int i = 0;
						     i != m_abscissaList.size() && m_abscissaList[i] <= distance; ++i)
							unextinguishedRaysCount[i] += 1.;

						// In the following, off-bound rays are processed differently: an off-bound ray is not extinct in the computational zone, and it can consequently not be absorbed or scattered
						if (!impactOffBound) {
							mui = fabs(direction.scalar(localNormal));

							// Update cumulative number of unabsorbed rays
							for (int i = 0;
							     i != m_abscissaList.size() && m_abscissaList[i] <= distance; ++i)
								unabsorbedRaysCount[i] += mui;

							// Update cumulative mui and mus values
							for (int i = 0; i != m_muiList.size() && m_muiList[i] <= mui; ++i) muiCumulated[i] += 1.;
							for (int i = 0; i != m_musList.size() && m_musList[i] <= mus; ++i) musCumulated[i] += 1.;
						}

						// At this point, the iteration process is over: the counter is incremented
						++m_nIterations;

					} else ++m_nFail; // if (nTries != N_MAX_TRIES_PER_MEDIUM)
				} // for (int iShots = 0; iShots != m_nShotsPerMedium; ++iShots)
			} // for (iMedium = 0; iMedium != nMediaPerGroup; ++iMedium)
		} // for (iGroup = 0; iGroup != m_nGroups; ++iGroup)

	} // void MonteCarlo::iterate(int nIterations)

// -----------------------------------------------------------------------------

	void MonteCarlo::getResultsSerialized(double *results, int *solverState) const {
		/*
		 * Collect results stored in a solver object and serialize them as 2 arrays.
		 * The resulting data is to be passed to a PostProcessor object, either directly
		 * or through MPI communications.
		 * Check PostProcessor::gather() for information on data structure.
		 */

		int offset = 0;

		for (int i = 0; i != m_nGroups; ++i) {
			for (int j = 0; j != m_abscissaResolution; ++j) {
				results[i * m_abscissaResolution + j] = m_resultsExtinction[i][j];
			}
		}

		offset += m_abscissaResolution * m_nGroups;

		for (int i = 0; i != m_nGroups; ++i) {
			for (int j = 0; j != m_abscissaResolution; ++j) {
				results[offset + i * m_abscissaResolution + j] = m_resultsAbsorption[i][j];
			}
		}

		offset += m_abscissaResolution * m_nGroups;

		for (int i = 0; i != m_nGroups; ++i) {
			for (int j = 0; j != m_angleResolution; ++j) {
				results[offset + i * m_angleResolution + j] = m_resultsMui[i][j];
			}
		}

		offset += m_angleResolution * m_nGroups;

		for (int i = 0; i != m_nGroups; ++i) {
			for (int j = 0; j != m_angleResolution; ++j) {
				results[offset + i * m_angleResolution + j] = m_resultsMus[i][j];
			}
		}

		offset += m_angleResolution * m_nGroups;

		for (int i = 0; i != m_nGroups; ++i) {
			results[offset + i] = m_abscissaList[i];
		}

		offset += m_abscissaResolution;

		for (int i = 0; i != m_nGroups; ++i) {
			results[offset + i] = m_muiList[i];
		}

		offset += m_angleResolution;

		for (int i = 0; i != m_nGroups; ++i) {
			results[offset + i] = m_musList[i];
		}

		solverState[0] = m_nIterations;
		solverState[1] = m_nFail;

	}

/// ----------------------------------------------------------------------------
/// ----------------------------------------------------------------------------

	inline void randomOrigin(Point &randomOrigin, CRandomMersenne &RandomGenerator) {
		randomOrigin.setCoordinates(RandomGenerator.Random() * 0.2 + 0.5,
		                            RandomGenerator.Random() * 0.2 + 0.5,
		                            RandomGenerator.Random() * 0.2 + 0.5);
	}

// -----------------------------------------------------------------------------

	inline void randomDirection(Vector &randomDirection, CRandomMersenne &RandomGenerator) {
		/*
		 * Stochastically determine a direction. WARNING: the direction distribution is not uniform per direction,
		 * but per solide angle unit.
		 * Therefore, mu = cos(theta) and phi are uniform.
		 * The spherical coordinates use theta in [0, Pi] and phi in [0, 2 Pi]
		 */

		double theta = acos(2. * (RandomGenerator.Random() - 0.5));
		double phi = 2. * M_PI * RandomGenerator.Random();

		randomDirection.setCoordinates(sin(theta) * cos(phi), sin(theta) * sin(phi), cos(theta));
		// The vector is readily normalized
	}

// -----------------------------------------------------------------------------

	int lineSphereIntersection(Point const &M, Vector const &u, Sphere const &S, Point &intersect1, Point &intersect2) {
		/*
		 * Determine intersection between the line (M, u) and the sphere S.
		 * The coordinates of the intersection points are stored in vectors intersect1 and intersect2.
		 * Return an integer equal to the number of intersections between the line and the sphere (0, 1 or 2). If there
		 * is only 1 intersection, then intersect1 = intersect2.
		 */

		double alpha = sqr(u.getX())
		               + sqr(u.getY())
		               + sqr(u.getZ());
		double beta = 2. * ((M.getX() - (S.getCenter()).getX()) * u.getX()
		                    + (M.getY() - (S.getCenter()).getY()) * u.getY()
		                    + (M.getZ() - (S.getCenter()).getZ()) * u.getZ()
		);
		double gamma = sqr(M.getX() - (S.getCenter()).getX())
		               + sqr(M.getY() - (S.getCenter()).getY())
		               + sqr(M.getZ() - (S.getCenter()).getZ())
		               - sqr(S.getRadius());
		double delta = sqr(beta) - (4. * alpha * gamma);
		int intersect;

		if (delta > 0) {
			intersect = 2;
		} else if (delta == 0) { // Discriminant nul
			intersect = 1;
		} else {
			intersect = 0;
		}

		if (intersect > 0) {
			double t1 = (-beta - sqrt(delta)) / (2. * alpha);
			double t2 = (-beta + sqrt(delta)) / (2. * alpha);

			intersect1 = M + u * t1;
			intersect2 = M + u * t2;
		}

		return intersect;
	}

// -----------------------------------------------------------------------------

	inline bool pointInTrans(Point const &point, Medium const &myMedium) {
		// Check if point is in the transparent phase.
		// For DOTS, a point is in the transparent phase if it is in at least one sphere STRICTLY (i.e. interface points
		// are not).
		for (SphereList::const_iterator it = (myMedium.getSphereList()).begin();
		     it != (myMedium.getSphereList()).end(); ++it) {
			if (pointInSphere(point, *it)) return true;
		}

		return false;
	}

// -----------------------------------------------------------------------------

	inline bool pointInSphere(Point const &point, Sphere const &sphere) {
		/*
		 * Return true if point is in strictly sphere, false otherwise. The comparison is made using a relative
		 * tolerance of 1e-6.
		 */

		return point.distance((sphere).getCenter()) < ((sphere).getRadius() - 1e-6 * (sphere).getRadius());
	}

// -----------------------------------------------------------------------------

	bool isOffBounds(Point &point) {
		/*
		 * Check if an intersection is off-bound; if it is, reset the coordinates to the edge of the bounding box
		 * (useful to compute the number of unextinct rays).
		 */

		if (   point.getX() < BOUND_XMIN
		    || point.getX() > BOUND_XMAX
		    || point.getY() < BOUND_YMIN
	        || point.getY() > BOUND_YMAX
		    || point.getZ() < BOUND_ZMIN
	        || point.getZ() > BOUND_ZMAX) {
			point.setCoordinates(max(min(point.getX(), BOUND_XMAX), BOUND_XMIN),
			                     max(min(point.getY(), BOUND_YMAX), BOUND_YMIN),
			                     max(min(point.getZ(), BOUND_ZMAX), BOUND_ZMIN));
			return true;
		} else {
			return false;
		}
	}
} // namespace dots
