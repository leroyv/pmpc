#include "test.h"
#include "MonteCarlo.h"
#include <string>
#include "Medium.h"

using namespace std;
using namespace dots;

void dots::testMonteCarlo() {
  cout << "*** Function testMonteCarlo() ***" << endl;

  string mediumTypeStr = "dots";
  double porosity = 0.6;
  string probLawStr = "dirac";
  double stdDeviation = 1e-1;
  double maxAbscissa = 5.0;

  Medium myMedium;
  myMedium.init(mediumTypeStr,porosity,probLawStr,stdDeviation,maxAbscissa);

  MonteCarlo myMCSolver;
  cout << "Initializing medium..." << endl;
  myMCSolver.initMedium(mediumTypeStr,porosity,probLawStr,stdDeviation,maxAbscissa);

  cout << "Initializing solver..." << endl;
  myMCSolver.initSolver(501,301,10,100);
  myMCSolver.reset();

  vector<vector<double> > myVector;


  for (int i = 0; i != 100; ++i) {
    myMCSolver.iterate(1000);

    cout << "Number of iterations: " << myMCSolver.getNIterations() << endl;
    cout << "Number of failures: " << myMCSolver.getNFail() << endl;
    cout << "StdDev on extinction: " << myMCSolver.getGroupStdDeviation()[0] << endl;
    cout << "StdDev on absorption: " << myMCSolver.getGroupStdDeviation()[1] << endl;
    cout << "StdDev on mui: " << myMCSolver.getGroupStdDeviation()[2] << endl;
    cout << "StdDev on mus: " << myMCSolver.getGroupStdDeviation()[3] << endl;
  }

  cout << "Ending iterations; post-processing..." << endl;

  myMCSolver.postProcess(1,1);

  cout << "Outputing results to output/dots..." << endl;

  myMCSolver.report("output/dots");

}
