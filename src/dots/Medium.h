/*
 * Classes related with spehre-based media (DOOS and DOTS)
 * Vincent Leroy
*/

#ifndef DEF_MEDIUM
#define DEF_MEDIUM

#include <string>
#include <boost/multi_array.hpp>

#include "../msm/geometry3d/Sphere.h"
#include "SphereList.h"
#include "../randomc/randomc.h"
#include "../utilities/filesystem/path.h"

namespace dots {
	class Medium {
		/*
		 * The Monte Carlo solver class is a friend so that it can access the RNG
		 */

	public:
		// Constructors
		Medium();
		Medium(int mediumTypeIndex, double porosity, int probLawIndex, double stdDeviation, double maxAbscissa);
		Medium(std::string mediumTypeStr, double porosity, std::string probLawStr, double stdDeviation,
		       double maxAbscissa);

		// Initialization method and overloads
		void init(int mediumTypeIndex, double porosity, int problawIndex, double stdDeviation, double maxAbscissa);

		void init(std::string const &mediumTypeStr, double porosity, std::string const &probLawStr, double stdDeviation,
		          double maxAbscissa);

		// Reset RNG
		inline void resetRandom() { m_RandomGenerator.RandomInit((int) time(0)); }

		// Generation (and overloads)
		void generate();
		void generate(int mediumTypeIndex, double porosity, int problawIndex, double stdDeviation, double maxAbscissa);
		void
		generate(std::string const &mediumTypeStr, double porosity, std::string const &probLawStr, double stdDeviation,
		         double maxAbscissa);

		// Mutators (and overloads)
		void addSphere(msm::geometry3d::Sphere &mySphere) { m_SphereList.push_back(mySphere); }
		void addSphere(double const x, double const y, double const z, double const r);
		void addSphere(std::string const &sphereStr);

		// Redimensioning (with edge artifact removal)
		void trim(double newMaxAbscissa);

		// Cleanup
		void clear();
		void voxelClear();

		// Save and reload
		void save(filesystem::path const &pathToSaveFile);
		void load(filesystem::path const &pathToLoadFile);

		// Export
		void exportToRaw(filesystem::path const &pathToSaveFile, int resolution);

		// Accessors
		int getMediumTypeIndex() const { return m_mediumTypeIndex; }
		std::string getMediumTypeStr() const;
		double getAverageRadius() const; // Returns sphere average radius
		double getAPlus() const; // Returns dimensionless specific surface area
		double getNPlus() const; // Returns dimensionless sphere number density
		int getNSpheresMath() const; // Returns theoretical sphere number density
		int getNSpheres() const; // Returns computed sphere number density
		double getPorosity() const { return m_porosity; } // Returns theoretical porosity
		int getProbLawIndex() const { return m_probLawIndex; } // Returns probability law identifier
		std::string getProbLawStr() const; // Returns probability law string name
		double getStdDeviation() const { return m_stdDeviation; } // Returns relative standard deviation
		double getMaxAbscissa() const { return m_maxAbscissa; } // Returns max abscissa
		const SphereList &getSphereList() const { return m_SphereList; } // Returns reference to the sphere list

		// Other member functions
		double newVolumeSize(double poreSize) const {
			return (*this).getAPlus() * poreSize / (4. * (*this).getAverageRadius());
		} // Return bounding box size if it would be dimensioned, using the typical pore size passed as an argument as the reference length


	private:
		// RNG for medium generation
		CRandomMersenne m_RandomGenerator;

		// Data used for medium generation
		int m_mediumTypeIndex; // Medium type (DOOS / DOTS)
		double m_porosity; // Porosity
		int m_probLawIndex; // Probability law for radius distribution
		double m_stdDeviation; // Standard deviation for probability law
		double m_maxAbscissa; // Maximum dimensionless abscissa

		// Data mathematically defining the medium
		SphereList m_SphereList;

		// Data defining the voxellized medium
		int m_voxelMediumResolution;
		boost::multi_array<char, 3> m_voxelMedium; // char is the smallest possible type
	};

	inline std::string mediumTypeIndex2Str(int mediumTypeIndex) {
		if (mediumTypeIndex == 0) { return "dots"; }
		else if (mediumTypeIndex == 1) { return "doos"; }
		else { return ""; }
	}


	inline int mediumTypeStr2Index(std::string const &mediumTypeStr) {
		if (mediumTypeStr.compare("dots") == 0) { return 0; }
		else if (mediumTypeStr.compare("doos") == 0) { return 1; }
		else { return -1; } // Return -1 is the argument is not recognized
	}


	inline std::string probLawIndex2Str(int probLawIndex) {
		if (probLawIndex == 0) { return "uniform"; }
		else if (probLawIndex == 1) { return "gauss"; }
		else if (probLawIndex == 2) { return "dirac"; }
		else { return ""; }
	}


	inline int probLawStr2Index(std::string const &probLawStr) {
		if (probLawStr.compare("uniform") == 0) { return 0; }
		else if (probLawStr.compare("gauss") == 0) { return 1; }
		else if (probLawStr.compare("dirac") == 0) { return 2; }
		else { return -1; }
	}

} // namespace dots

#endif


