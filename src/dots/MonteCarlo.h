/** Cette classe encapsule un solveur Monte Carlo permettant de déterminer
  * les fonctions de distribution radiatives dans un milieu DOTS.
  */

#ifndef DEF_DOTS_MONTECARLO
#define DEF_DOTS_MONTECARLO

// Standard headers
#include <vector>

// DOTS
#include "Medium.h"

// RNG
#include "../randomc/randomc.h"

// Math
#include "../msm/msm.h"


namespace dots {
	// Constants used for some functions
	static double const BOUND_XMIN = 0., BOUND_XMAX = 1.;
	static double const BOUND_YMIN = 0., BOUND_YMAX = 1.;
	static double const BOUND_ZMIN = 0., BOUND_ZMAX = 1.;


	class MonteCarlo {
		// Public member functions
	public:

		// Constructors
		MonteCarlo();

		// Initialization
		void initRandomGenerator(int seed); // Reset RNG
		void initMedium(std::string const &mediumTypeStr, double porosity, std::string const &probLawStr,
		                double stdDeviation, double maxAbscissa);

		void initSolver(int abscissaResolution, int angleResolution, int nGroups, int nShotsPerMedium);

		// Reset solver (and RNG)
		void reset();

		// Purge results
		void purge();

		// iterate
		void iterate(int nIterations);

		// Gather raw data
		const std::vector<double> &getAbscissaList() const { return m_abscissaList; }
		const std::vector<std::vector<double> > &getResultsExtinction() const { return m_resultsExtinction; }
		const std::vector<std::vector<double> > &getResultsAbsorption() const { return m_resultsAbsorption; }
		const std::vector<std::vector<double> > &getResultsMui() const { return m_resultsMui; }
		const std::vector<std::vector<double> > &getResultsMus() const { return m_resultsMus; }
		void getResultsSerialized(double *results, int *solverState) const;

		// Gather state info
		std::vector<double> getGroupStdDeviation();
		int getNIterations() const { return m_nIterations; }
		int getNFail() const { return m_nFail; }
		void getStateInfoSerialized(int *stateInfo);

		// Private attributes
	private:

		// Computational parameters
		int m_abscissaResolution;
		int m_angleResolution;
		int m_nGroups;
		int m_nShotsPerMedium;

		// Solver state info
		int m_nIterations; // Number of successful iterations
		int m_nFail; // Number of failed iterations

		// Results containers
		std::vector<std::vector<double> > m_resultsExtinction;
		std::vector<std::vector<double> > m_resultsAbsorption;
		std::vector<std::vector<double> > m_resultsMui;
		std::vector<std::vector<double> > m_resultsMus;
		std::vector<double> m_abscissaList;
		std::vector<double> m_muiList;
		std::vector<double> m_musList;

		// DOTS medium
		Medium m_Medium;

		// RNG
		CRandomMersenne m_RandomGenerator;

	};

	// Functions used in the MonteCarlo class's member functions
	inline void randomOrigin(msm::geometry3d::Point &randomOrigin, CRandomMersenne &RandomGenerator);
	inline void randomDirection(msm::geometry3d::Vector &randomDirection, CRandomMersenne &RandomGenerator);
	inline bool pointInTrans(msm::geometry3d::Point const &point, Medium const &myMedium);
	inline bool pointInSphere(msm::geometry3d::Point const &point, msm::geometry3d::Sphere const &sphere);
	int lineSphereIntersection(msm::geometry3d::Point const &M, msm::geometry3d::Vector const &u,
	                           msm::geometry3d::Sphere const &S, msm::geometry3d::Point &intersect1,
	                           msm::geometry3d::Point &intersect2);
	bool isOffBounds(msm::geometry3d::Point &point);

} // namespace dots

#endif
