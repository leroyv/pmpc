/*
 * Classes related with sphere fields for DOTS media
 * Vincent Leroy
 */

#ifndef DEF_SPHERELIST
#define DEF_SPHERELIST

#include <list>
#include "../msm/msm.h"
#include "../randomc/randomc.h"

using msm::geometry3d::Sphere;

// Medium sphere list
namespace dots {
	class SphereList : public std::list<Sphere> {
	public:
		// Constructors
		SphereList() : std::list<Sphere>() {}

		// Other member functions
		void generate(int probLawIndex, double averageRadius, double stdDeviation, int nbSpheres,
		              CRandomMersenne &RandomGenerator);

		void print() const;
	};
}

#endif

