/** Testing functions for the Monte Carlo solver for DOTS media
  */

#ifndef DEF_DOTS_TEST
#define DEF_DOTS_TEST

namespace dots {
  void testMonteCarlo();
}

#endif
