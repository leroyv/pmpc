#include "SphereList.h"
#include "../utilities/convert.h"

using std::cout;
using std::endl;
using std::max;
using msm::misc::sign;

/**
    =================
    SphereList Class
    =================
*/

namespace dots {
	void SphereList::generate(int probLawIndex, double averageRadius, double stdDeviation, int nbSpheres,
	                          CRandomMersenne &RandomGenerator) {
		/*
		 * Generate a sphere field based on a specified probability law, using the specified mean radius.
		 * Clears the sphere list. The RNG passed as an argument must be initialized.
		 */

		(*this).clear(); // Purge the list

		double radius = 0;
		msm::geometry3d::Sphere mySphere;

		for (int i = 0; i < nbSpheres; ++i) {

			switch (probLawIndex) { // Determine radius
				case 0: // Uniform
					radius = averageRadius * (1. + stdDeviation * (1. - 2. * RandomGenerator.Random()));
					break;

				case 1: // Gaussian (cf Box-Muller method)
					radius = averageRadius * (1. + stdDeviation
					                               * sqrt(-2. * log(RandomGenerator.Random()))
					                               * cos(2. * M_PI * RandomGenerator.Random())
					);
					break;

				case 2: // Dirac
					radius = averageRadius;
					break;
			}

			// Create sphere and add it to the field
			mySphere.setAll(RandomGenerator.Random(), RandomGenerator.Random(), RandomGenerator.Random(),
			                max(0.0, radius)); // Remove irrelevant data (negative radii)
			(*this).push_back(mySphere);
		}
	}


	void SphereList::print() const {
		for (std::list<msm::geometry3d::Sphere>::const_iterator it = (*this).begin(); it != (*this).end(); ++it) {
			cout << (*it) << endl;
		}
	}
}
