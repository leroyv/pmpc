/**
 * \file MPI.h
 *
 * \brief A header-only C++ MPI wrapper for the PMPC library
 * \author Vincent Leroy
 *
 *
 */

#ifndef PMPC_MPICOM_H
#define PMPC_MPICOM_H

#include <mpi.h>
#include <string>
#include <map>

/**
 * \namespace MPICom
 *
 * A namespace containing all the MPI communication facilities
 */
namespace MPICom {

/**
 * \class MessageType
 *
 * \brief A class representing a message type
 */
struct MessageType {
	int tag; ///< Unique tag
	MPI_Datatype mpi_datatype; ///< Associated MPI datatype
	std::string name; ///< Human-readable name

	MessageType(int tag, MPI_Datatype mpi_datatype, std::string name) {
		this->tag = tag;
		this->mpi_datatype = mpi_datatype;
		this->name = name;
	}
};

static const MessageType MESSAGETYPE_DATA_INT(0, MPI_INT, "DATA_INT");
static const MessageType MESSAGETYPE_DATA_DOUBLE(1, MPI_DOUBLE, "DATA_DOUBLE");
static const MessageType MESSAGETYPE_DATA_LONG_LONG_INT(2, MPI_LONG_LONG_INT, "DATA_LONG_LONG_INT");
static const MessageType MESSAGETYPE_ORDER(3, MPI_INT, "ORDER");
static const MessageType MESSAGETYPE_REPORT(4, MPI_INT, "REPORT");

/**
 * \enum Order
 *
 * \brief Contains codes for order transmission from master to slave
 */
enum class Order : int {
    COMPUTE, ///< Compute
    STOP ///< Stop
};

/**
 * \class MPI
 *
 * \brief A C++ MPI wrapper
 *
 * This class contains a variety of MPI commands, wrapped with a more convenient C++
 * interface for better code legibility.
 */
class MPI {

public:

    /// \name Constructors
    ///@{
    /// Default constructor
    MPI() { init(); }
    ///@}

    /// \name Destructors
    ///@{
    /// Default destructor
    ~MPI() { finalize(); }
    ///@}

    /// \name Senders
    ///@{
    /**
     * \brief Send a message to the target process
     *
     * \param message_type: MessageType object identifying the type of message being tramsitted
     * \param rank_dst: rank of the destination process
     * \param p_array: pointer to the first element of the array to be transmitted
     * \param size: size of the array to be transmitted
     */
	template<typename T>
    void send(const MessageType &message_type, int rank_dst, T *p_array, int size) {
        MPI_Send(
            p_array,
            size,
            message_type.mpi_datatype,
            rank_dst,
            message_type.tag,
            MPI_COMM_WORLD
        );
    }
    ///@}

    /// \name Receivers
    ///@{
    /**
     * \brief Receive a message from the target process
     *
     * \param message_type: MessageType object identifying the type of message being tramsitted
     * \param rank_src: rank of the source process
     * \param p_array: pointer to the first element of the array where data will be stored (must be big enough,
     * size can be probed with the get_size function)
     * \param size: size of the array transmitted
     */
	template<typename T>
    void recv(const MessageType &message_type, int rank_src, T *p_array, int size) {
        MPI_Recv(
            p_array,
            size,
            message_type.mpi_datatype,
            rank_src,
            message_type.tag,
            MPI_COMM_WORLD,
            MPI_STATUS_IGNORE
        );
    }
    ///@}

    /**
     * \brief Access rank of the current process
     *
     * \return int rank of the current process
     */
    const int rank() const { return m_rank; }

    /**
     * \brief Access communicator size
     *
     * \return int communicator size
     */
    const int world_size() const { return m_world_size; }

private:

    /// Initialize MPI communications
    void init() {
        MPI_Init(NULL, NULL);
        MPI_Comm_rank(MPI_COMM_WORLD, &m_rank);
        MPI_Comm_size(MPI_COMM_WORLD, &m_world_size);
    }

    /// Finalize MPI communications
    void finalize() { MPI_Finalize(); }

    /**
     * \brief Rank of the current process
     */
    int m_rank;

    /**
     * \brief Communicator size
     */
    int m_world_size;

};

} // namespace MPICom


#endif //PMPC_MPICOM_H
