#!/usr/bin/perl

@program_names = ("computeGe");
%program_nodes = ("computeGe", 8);

$program_to_run = $ARGV[0];
if (!$program_to_run || !$program_nodes{$program_to_run}) {
  die "Must enter program name to run. Possible programs are: " .
      "\n@program_names\n";
} else {
  if ($ENV{"MPIRUN"}) {
    $mpirun = $ENV{"MPIRUN"};
  } else {
    $mpirun = "mpiexec";
  }
  if ($ENV{"MPI_HOSTS"}) {
    $hosts = "-f " . $ENV{"MPI_HOSTS"};
  } else {
    $hosts = "";
  }

  if ($program_to_run == "computeGe") {
    $args = "input_files/computeGe.conf"
  }

  print "$mpirun -n $program_nodes{$program_to_run} $hosts ./bin/Release/$program_to_run $args\n";
  system("$mpirun -n $program_nodes{$program_to_run} $hosts ./bin/Release/$program_to_run $args");
}
